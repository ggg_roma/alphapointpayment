﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using BaseApp.Data.DataContext;
using BaseApp.Data.Migration.Migrations;

namespace BaseApp.Data.Migration
{
    public class DbDataMigrateValidateOnly<TContext> : IDatabaseInitializer<TContext> where TContext : DBData
    {
        public void InitializeDatabase(TContext context)
        {
            if (!context.Database.Exists())
                throw new Exception("Database does not exists");

            //var validModel = new DBData().Database.CompatibleWithModel(true);//TODO: check schema

            var migrator = new DbMigrator(new Configuration());
            var dbMigrations = migrator.GetDatabaseMigrations().ToList();
            var localMigrations = migrator.GetLocalMigrations().ToList();

            var dbPendings = dbMigrations.Except(localMigrations).ToList();
            var error = "";
            if (dbPendings.Any())
            {
                error += string.Format("Database contains migrations which not exists in code. List: {0}", string.Join(", ", dbPendings));
            }
            var localPendings = localMigrations.Except(dbMigrations).ToList();
            if (localPendings.Any())
            {
                error += string.Format("Code contains migrations which not exists in Database. List: {0}", string.Join(", ", localPendings));
            }

            if (!string.IsNullOrWhiteSpace(error))
            {
                var latestDbMigration = "NONE";
                if (dbMigrations.Any())
                {
                    latestDbMigration = dbMigrations.First();
                }
                error += Environment.NewLine + string.Format("Latest Db Migration: {0}", latestDbMigration);

                throw new Exception(error);
            }
                
        }
    }
}
