namespace BaseApp.Data.Migration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RazorpayPaymentEntityChanged : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RazorpayPayment", "ResponseObject", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RazorpayPayment", "ResponseObject", c => c.String(unicode: false));
        }
    }
}
