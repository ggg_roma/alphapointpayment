namespace BaseApp.Data.Migration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class detectChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TrustPayPayment", "ModifiedUtc", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TrustPayPayment", "ModifiedUtc");
        }
    }
}
