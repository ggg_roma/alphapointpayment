namespace BaseApp.Data.Migration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RazorpayPaymentEntityFieldTypeChanged : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RazorpayPayment", "ResponseObject", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RazorpayPayment", "ResponseObject", c => c.String(maxLength: 512));
        }
    }
}
