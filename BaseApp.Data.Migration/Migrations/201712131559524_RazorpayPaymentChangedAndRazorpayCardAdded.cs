namespace BaseApp.Data.Migration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RazorpayPaymentChangedAndRazorpayCardAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RazorpayCard",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RazorpayCardId = c.String(maxLength: 512),
                        Entity = c.String(maxLength: 512),
                        Name = c.String(maxLength: 512),
                        Last4 = c.Int(nullable: false),
                        Network = c.String(maxLength: 512),
                        Type = c.String(maxLength: 512),
                        Issuer = c.String(maxLength: 512),
                        International = c.Boolean(nullable: false),
                        Emi = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.RazorpayPayment", "RazorpayPaymentId", c => c.String(maxLength: 512));
            AddColumn("dbo.RazorpayPayment", "CreateDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.RazorpayPayment", "Fee", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.RazorpayPayment", "Tax", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.RazorpayPayment", "ErrorCode", c => c.String(maxLength: 512));
            AddColumn("dbo.RazorpayPayment", "ErrorDescription", c => c.String());
            AddColumn("dbo.RazorpayPayment", "CreatedAt", c => c.String(maxLength: 512));
            AddColumn("dbo.RazorpayPayment", "ResponseObject", c => c.String(maxLength: 512));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RazorpayPayment", "ResponseObject");
            DropColumn("dbo.RazorpayPayment", "CreatedAt");
            DropColumn("dbo.RazorpayPayment", "ErrorDescription");
            DropColumn("dbo.RazorpayPayment", "ErrorCode");
            DropColumn("dbo.RazorpayPayment", "Tax");
            DropColumn("dbo.RazorpayPayment", "Fee");
            DropColumn("dbo.RazorpayPayment", "CreateDate");
            DropColumn("dbo.RazorpayPayment", "RazorpayPaymentId");
            DropTable("dbo.RazorpayCard");
        }
    }
}
