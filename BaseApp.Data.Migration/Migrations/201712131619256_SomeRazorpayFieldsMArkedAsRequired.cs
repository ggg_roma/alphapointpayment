namespace BaseApp.Data.Migration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeRazorpayFieldsMArkedAsRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RazorpayCard", "RazorpayCardId", c => c.String(nullable: false, maxLength: 512));
            AlterColumn("dbo.RazorpayPayment", "RazorpayPaymentId", c => c.String(nullable: false, maxLength: 512));
            AlterColumn("dbo.RazorpayPayment", "Status", c => c.String(nullable: false, maxLength: 512));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RazorpayPayment", "Status", c => c.String(maxLength: 512));
            AlterColumn("dbo.RazorpayPayment", "RazorpayPaymentId", c => c.String(maxLength: 512));
            AlterColumn("dbo.RazorpayCard", "RazorpayCardId", c => c.String(maxLength: 512));
        }
    }
}
