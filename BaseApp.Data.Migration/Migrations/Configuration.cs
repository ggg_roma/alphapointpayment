using System.Data.Entity.Migrations;
using System.Linq;

namespace BaseApp.Data.Migration.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<BaseApp.Data.DataContext.DBData>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Data.DataContext.DBData context)
        {
        }
    }
}
