namespace BaseApp.Data.Migration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RazorpayPaymentModelAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RazorpayPayment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Entity = c.String(maxLength: 512),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Currency = c.String(maxLength: 512),
                        Status = c.String(maxLength: 512),
                        OrderId = c.String(maxLength: 512),
                        InvoiceId = c.String(maxLength: 512),
                        International = c.Boolean(nullable: false),
                        Method = c.String(maxLength: 512),
                        AmountRefunded = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RefundStatus = c.String(maxLength: 512),
                        Captured = c.Boolean(nullable: false),
                        Description = c.String(),
                        CardId = c.String(maxLength: 512),
                        Bank = c.String(maxLength: 512),
                        Wallet = c.String(maxLength: 512),
                        VPA = c.String(maxLength: 512),
                        Email = c.String(maxLength: 512),
                        Contact = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.RazorpayPayment");
        }
    }
}
