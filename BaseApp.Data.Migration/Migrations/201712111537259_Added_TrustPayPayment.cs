namespace BaseApp.Data.Migration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_TrustPayPayment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TrustPayPayment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CreationTime = c.DateTime(nullable: false),
                        CheckoutId = c.String(nullable: false, maxLength: 512),
                        Currency = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserAccountId = c.String(maxLength: 512),
                        PaymentStatus = c.Int(nullable: false),
                        SubmittedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TrustPayPayment");
        }
    }
}
