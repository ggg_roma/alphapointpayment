namespace BaseApp.Data.Migration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TrustPayApiResultDescription_Added : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TrustPayPayment", "TrustPayApiResultDescription", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TrustPayPayment", "TrustPayApiResultDescription");
        }
    }
}
