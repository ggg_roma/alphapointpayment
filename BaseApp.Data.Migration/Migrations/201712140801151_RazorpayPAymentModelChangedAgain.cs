namespace BaseApp.Data.Migration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RazorpayPAymentModelChangedAgain : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RazorpayPayment", "ResponseObject", c => c.String(maxLength: 5000));
        }

        public override void Down()
        {
            AlterColumn("dbo.RazorpayPayment", "ResponseObject", c => c.String());
        }
    }
}
