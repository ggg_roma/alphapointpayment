﻿using System.ComponentModel;
using System.Data.Entity.Migrations;
using System.Linq;
using BaseApp.Common;
using BaseApp.Data.DataContext;
using BaseApp.Data.Migration.Migrations;

namespace BaseApp.Data.Migration
{
    public static class DbInitializer
    {
        public static void Init(Enums.DbDataInitStrategyTypes strategyType)
        {
            switch (strategyType)
            {
                case Enums.DbDataInitStrategyTypes.MigrateToLatest:
                    DBData.SetInitializer(new DbDataMigrateToLatestInitializer());
                    return;
                case Enums.DbDataInitStrategyTypes.MigrateValidate:
                    DBData.SetInitializer(new DbDataMigrateValidateOnly<DBData>());
                    return;
                case Enums.DbDataInitStrategyTypes.None:
                    DBData.SetInitializer(null);
                    return;
                default:
                    throw new InvalidEnumArgumentException("strategyType");
            }
        }

        public static string GetLatestMigration()
        {
            var configurator = new DbMigrator(new Configuration());
            return configurator.GetLocalMigrations().LastOrDefault() ?? "None";
        }
    }
}
