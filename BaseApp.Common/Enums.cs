﻿using System.ComponentModel;

namespace BaseApp.Common
{
    public class Enums
    {
        public enum NotificationEmailTypes
        {
            ResetPassword = 1
        }

        public enum MenuItemTypes
        {
            
        }

        public enum DbDataInitStrategyTypes
        {
            MigrateValidate,
            MigrateToLatest,
            None
        }

        public enum SchedulerOnDateShiftTypes
        {
            //TODO: implement
        }

        public enum Currencies
        {
            [Description("US Dollar")]
            USD = 1,
            [Description("Canadian Dollar")]
            CAD,
            [Description("Euro")]
            EUR
        }

        public enum PaymentStatuses
        {
            New = 1,
            Submitted,
            Error,
            WrongComparedData
        }

        /// <summary>
        /// Represents a payment status enumeration of PCiGate
        /// </summary>
        public enum PaymentStatusPCiGate : int
        {
            /// <summary>
            /// Pending
            /// </summary>
            Pending = 10,
            /// <summary>
            /// Authorized
            /// </summary>
            Authorized = 20,
            /// <summary>
            /// Paid
            /// </summary>
            Paid = 30,
            /// <summary>
            /// Partially Refunded
            /// </summary>
            PartiallyRefunded = 35,
            /// <summary>
            /// Refunded
            /// </summary>
            Refunded = 40,
            /// <summary>
            /// Voided
            /// </summary>
            Voided = 50,
        }

    }
}
