﻿using System;
using System.Data.Entity.Validation;
using System.Text;

namespace BaseApp.Data.Exceptions
{
    /// <summary>
    /// used to extend Message property with validation errors info
    /// </summary>
    public class DbEntityValidationExceptionWrapper : Exception
    {
        internal DbEntityValidationExceptionWrapper(DbEntityValidationException exception)
            : base(GetMessage(exception), exception)
        {
        }

        private static string GetMessage(DbEntityValidationException exception)
        {
            var errorStringBilder = new StringBuilder();
            errorStringBilder.AppendLine(exception.Message);

            foreach (var entityValidationError in exception.EntityValidationErrors)
            {
                errorStringBilder.AppendLine("-------------------------------------------");
                errorStringBilder.AppendLine(string.Format("Entity \"{0}\" in state \"{1}\", errors:",
                        entityValidationError.Entry.Entity.GetType().Name,
                        entityValidationError.Entry.State));

                foreach (var error in entityValidationError.ValidationErrors)
                {
                    errorStringBilder.AppendLine(
                    string.Format(" {0}: \"{1}\"",
                    error.PropertyName, error.ErrorMessage));
                }
            }

            return errorStringBilder.ToString();
        }
    }
}
