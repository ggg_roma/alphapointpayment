﻿using BaseApp.Data.DataContext;

namespace BaseApp.Data.Infrastructure
{
    public class StoredProcedureProvider
    {
        private DBData Context { get; set; }

        public StoredProcedureProvider(DBData context)
        {
            Context = context;
        }

        /*         
         * Sample of usage
         
         var firstP = new SqlParameter("@First", 5);
         var secondP = new SqlParameter("@Second", true);
         Context.Database.SqlQuery<ProcedureResult>("sp_TestProcedure @First @Second", firstP, secondP).ToList();
         
         */
    }
}
