﻿using BaseApp.Common;
using System;
using System.ComponentModel.DataAnnotations;


namespace AlphaPointPayment.Data.DataContext.Entities
{
    public class TrustPayPayment
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreationTime { get; set; }

        [Required]
        public string CheckoutId { get; set; }

        [Required]
        public Enums.Currencies Currency { get; set; }

        public decimal Amount { get; set; }

        public string UserAccountId { get; set; }

        public Enums.PaymentStatuses PaymentStatus { get; set; }

        public DateTime? SubmittedTime { get; set; }

        public DateTime ModifiedUtc { get; set; }

        public string TrustPayApiResultDescription { get; set; }
    }
}
