﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using BaseApp.Data.DataContext.Interfaces;
using BaseApp.Data.Infrastructure;

namespace BaseApp.Data.DataContext.Entities
{
    public class User : IDeletable
    {
        public User()
        {
            Roles = new HashSet<Role>();
            Attachments = new HashSet<Attachment>();
            UpdatedUsers = new HashSet<User>();
            UserForgotPasswords = new HashSet<UserForgotPassword>();
            DeletedUsers = new HashSet<User>();
        }

        public int Id { get; set; }

        [Required, StringLength(64), Index("IX_LoginIndex", IsUnique = true, Order = 0)]
        public string Login { get; set; }

        [Required, StringLength(256)]
        public string Password { get; set; }

        [Required, StringLength(64)]
        public string FirstName { get; set; }

        [Required, StringLength(64)]
        public string LastName { get; set; }

        [Required, StringLength(64)]
        public string Email { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int? UpdatedByUserId { get; set; }

        [Index("IX_LoginIndex", Order = 1)]
        public DateTime? DeletedDate { get; set; }

        public int? DeletedByUserId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string FullName
        {
            get { return string.Format("{0} {1}", FirstName, LastName).Trim(); }
            private set { }
        }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
        [ForeignKey("UpdatedByUserId")]
        public virtual ICollection<User> UpdatedUsers { get; set; }
        public virtual User UpdatedByUser { get; set; }
        public virtual ICollection<UserForgotPassword> UserForgotPasswords { get; set; }
        [ForeignKey("DeletedByUserId")]
        public virtual ICollection<User> DeletedUsers { get; set; }
        public virtual User DeletedByUser { get; set; }
    }

    internal class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            HasMany(s => s.Roles).WithMany(c => c.Users)
                .Map(cs => { cs.MapLeftKey("UserId"); cs.MapRightKey("RoleId"); cs.ToTable("UserRole"); });
        }
    }
}
