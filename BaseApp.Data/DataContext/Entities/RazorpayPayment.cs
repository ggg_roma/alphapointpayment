﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlphaPointPayment.Data.DataContext.Entities
{
    public class RazorpayPayment
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string RazorpayPaymentId { get; set; }
        public string Entity { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        [Required]
        public string Status { get; set; }
        public string OrderId { get; set; }
        public string InvoiceId { get; set; }
        public bool International { get; set; }
        public string Method { get; set; }
        public decimal AmountRefunded { get; set; }
        public string RefundStatus { get; set; }
        public bool Captured { get; set; }
        public string Description { get; set; }
        public string CardId { get; set; }
        public string Bank { get; set; }
        public string Wallet { get; set; }
        public string VPA { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public DateTime CreateDate { get; set; }
        public decimal Fee { get; set; }
        public decimal Tax { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
        public string CreatedAt { get; set; }

        [MaxLength(5000)]
        public string ResponseObject { get; set; }
    }
}
