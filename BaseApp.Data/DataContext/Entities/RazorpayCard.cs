﻿using System.ComponentModel.DataAnnotations;

namespace AlphaPointPayment.Data.DataContext.Entities
{
    public class RazorpayCard
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string RazorpayCardId { get; set; }
        public string Entity { get; set; }
        public string Name { get; set; }
        public int Last4 { get; set; }
        public string Network { get; set; }
        public string Type { get; set; }
        public string Issuer { get; set; }
        public bool International { get; set; }
        public bool Emi { get; set; }
    }
}
