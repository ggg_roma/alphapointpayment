﻿using AlphaPointPayment.Data.DataContext.Entities;
using BaseApp.Data.Infrastructure;

namespace AlphaPointPayment.Data.DataRepository
{
    public class RazorpayPaymentRepository : RepositoryEntityBase<RazorpayPayment>
    {
        public RazorpayPaymentRepository(DataContextProvider context) : base(context) { }
    }
}
