﻿using AlphaPointPayment.Data.DataContext.Entities;
using BaseApp.Data.Infrastructure;

namespace AlphaPointPayment.Data.DataRepository
{
    public class RazorpayCardRepository : RepositoryEntityBase<RazorpayCard>
    {
        public RazorpayCardRepository(DataContextProvider context) : base(context) { }
    }
}
