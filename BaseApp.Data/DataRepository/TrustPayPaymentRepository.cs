﻿using System;
using BaseApp.Common.Utils;
using BaseApp.Data.DataContext.Entities;
using BaseApp.Data.FileManagers;
using BaseApp.Data.Infrastructure;
using BaseApp.Data.Transaction;
using AlphaPointPayment.Data.DataContext.Entities;
using System.Linq;

namespace BaseApp.Data.DataRepository
{
    public class TrustPayPaymentRepository : RepositoryEntityBase<TrustPayPayment>
    {
        public TrustPayPaymentRepository(DataContextProvider context)
            : base(context)
        {
        }

        public TrustPayPayment GetByCheckoutIdOrNull(string checkoutId)
        {
            return EntitySet.FirstOrDefault(x => x.CheckoutId == checkoutId);
        }

    }
}
