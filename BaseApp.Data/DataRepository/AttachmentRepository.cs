﻿using System;
using BaseApp.Common.Utils;
using BaseApp.Data.DataContext.Entities;
using BaseApp.Data.FileManagers;
using BaseApp.Data.Infrastructure;
using BaseApp.Data.Transaction;

namespace BaseApp.Data.DataRepository
{
    public class AttachmentRepository : RepositoryEntityBase<Attachment>
    {
        public AttachmentRepository(DataContextProvider context)
            : base(context)
        {
        }

        public Attachment CreateAttachment(int userId, string fileName, byte[] content, ITransactionWrapper tran)
        {
            string genFileName = FileManagerFactory.Attachments.SaveFileWithUniqueName(fileName, content, tran);

            var attachment = CreateEmpty();
            attachment.CreatedByUserId = userId;
            attachment.CreatedDate = DateTime.Now;
            attachment.ContentType = MimeTypeResolver.Resolve(fileName);
            attachment.FileSize = content.Length;
            attachment.FileName = fileName;
            attachment.GenFileName = genFileName;

            return attachment;
        }

        public void DeleteAttachment(Attachment attachment, ITransactionWrapper tran)
        {
            if (!String.IsNullOrWhiteSpace(attachment.GenFileName))
            {
                FileManagerFactory.Attachments.DeleteFile(attachment.GenFileName, tran);
            }
            MarkForDelete(attachment);
        }
    }
}
