﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseApp.Data.DataContext;
using BaseApp.Data.Infrastructure.Data;

namespace BaseApp.Data.DataRepository
{
    public class SchedulerRepository : RepositoryEntityBase<Scheduler>
    {
        public SchedulerRepository(DataContextProvider context) : base(context)
        {
        }
    }
}
