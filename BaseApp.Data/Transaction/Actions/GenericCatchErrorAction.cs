﻿using System;
using BaseApp.Common;

namespace BaseApp.Data.Transaction.Actions
{
    public class GenericCatchErrorAction : ActionBase
    {
        private Action Action { get; set; }
        private string CatchErrorMessage { get; set; }

        public GenericCatchErrorAction(Action action, string catchErrorMessage)
        {
            if (action == null)
                throw new ArgumentNullException("action");
            if (String.IsNullOrWhiteSpace(catchErrorMessage))
                throw new ArgumentNullException("catchErrorMessage");

            Action = action;
            CatchErrorMessage = catchErrorMessage;
        }

        public override void Execute()
        {
            try
            {
                Action();
            }
            catch (Exception ex)
            {
                LogHolder.MainLog.ErrorException(CatchErrorMessage, ex);
            }
        }
    }
}
