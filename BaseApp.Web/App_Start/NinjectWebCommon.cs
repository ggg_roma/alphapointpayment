using System.Web.Http;
using BaseApp.Common.Extensions;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Code.Extensions;
using BaseApp.Web.Code.NinjectWebApi;
using BaseApp.Web.Models;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(BaseApp.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(BaseApp.Web.App_Start.NinjectWebCommon), "Stop")]

namespace BaseApp.Web.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel(new NinjectSettings() { AllowNullInjection = true });
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);

                GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(kernel); 

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IUnitOfWorkFactoryPerCall>().To<UnitOfWorkFactory>();
            kernel.Bind<IUnitOfWorkFactoryPerRequest>().To<UnitOfWorkFactory>().InRequestScope();
            kernel.Bind<LoggedUserInfo>().ToMethod(ctx =>
            {
                if (HttpContext.Current != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    return HttpContext.Current.User.GetUserInfo();
                return null;
            });
        }        
    }
}
