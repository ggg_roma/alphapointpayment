﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http.Description;
using System.Web.Mvc;


namespace BaseApp.Web.Areas.HelpPage.Code
{
    public static class CustomChanges
    {
        public static void RegisterTestRoute(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HelpPage_Test",
                "help/test/{action}/{apiId}",
                new { controller = "Test", apiId = UrlParameter.Optional });
        }

        public static void FilterFormatters(Type type, Collection<MediaTypeFormatter> formatters)
        {
            if (formatters != null && formatters.Count > 0)
            {
                Type formatterTypeAllowed = typeof(JsonMediaTypeFormatter);
                if (type != null && type.GetInterface(typeof(IMultipartData).Name) != null)
                {
                    formatterTypeAllowed = typeof(MultipartDataMediaFormatter.FormMultipartEncodedMediaTypeFormatter);
                }
                foreach (var fToRemove in formatters.Where(f => (f.GetType() != formatterTypeAllowed)).ToList())
                {
                    formatters.Remove(fToRemove);
                }
            }
        }

        public static string GetTestUrl(System.Web.Mvc.UrlHelper url, ApiDescription apiDescription)
        {
            return url.Action("ApiTest", "Test", new { apiId = apiDescription.GetFriendlyId() });
        }
    }
}