﻿
namespace AlphaPointPayment.Web.Models.Api.Polipayment
{
    public class GetTransactionInput
    {
        public string TransactionToken { get; set; }
    }
}