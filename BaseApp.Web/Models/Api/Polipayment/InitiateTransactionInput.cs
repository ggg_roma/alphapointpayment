﻿using BaseApp.Common;
using BaseApp.Web.Code.Infrastructure;
using System.ComponentModel.DataAnnotations;

namespace AlphaPointPayment.Web.Models.Api.Polipayment
{
    public class InitiateTransactionInput
    {
        [NotDefaultValueRequired]
        public decimal Amount { get; set; }

        [Required]
        public Enums.Currencies Currency { get; set;}
    }
}