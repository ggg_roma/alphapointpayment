﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlphaPointPayment.Web.Models.Api.PSiGate
{
    public class PSiGateBillingAddress
    {
        public string FirstName { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string ZipPostalCode { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
    }
}