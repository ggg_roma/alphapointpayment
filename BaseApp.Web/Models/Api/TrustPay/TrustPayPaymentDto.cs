﻿using BaseApp.Common;
using System;

namespace AlphaPointPayment.Web.Models.Api.TrustPay
{
    public class TrustPayPaymentDto
    {
        public int Id { get; set; }

        public DateTime CreationTime { get; set; }

        public string CheckoutId { get; set; }

        public Enums.Currencies Currency { get; set; }

        public decimal Amount { get; set; }

        public string UserAccountId { get; set; }

        public Enums.PaymentStatuses PaymentStatus { get; set; }

        public DateTime? SubmittedTime { get; set; }
    }
}