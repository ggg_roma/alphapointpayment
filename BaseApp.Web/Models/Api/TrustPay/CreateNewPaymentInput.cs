﻿using BaseApp.Common;
using System.ComponentModel.DataAnnotations;

namespace AlphaPointPayment.Web.Models.Api.TrustPay
{
    public class CreateNewPaymentInput
    {
        [Required]
        public string CheckoutId { get; set; }

        public Enums.Currencies Currency { get; set; }

        public decimal Amount { get; set; }

        public string UserAccountId { get; set; }
    }
}