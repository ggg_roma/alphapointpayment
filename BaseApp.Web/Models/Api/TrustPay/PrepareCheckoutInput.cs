﻿using BaseApp.Common;

namespace AlphaPointPayment.Web.Models.Api.TrustPay
{
    public class PrepareCheckoutInput
    {
        public decimal Amount { get; set; }

        public Enums.Currencies Currency { get; set; }
    }
}