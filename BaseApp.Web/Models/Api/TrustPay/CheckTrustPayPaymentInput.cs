﻿using System.ComponentModel.DataAnnotations;

namespace AlphaPointPayment.Web.Models.Api.TrustPay
{
    public class CheckTrustPayPaymentInput
    {
        [Required]
        public string resourcePath { get; set; }
		
		[Required]
		public string checkoutId { get; set; }
    }
}