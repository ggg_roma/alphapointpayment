﻿using BaseApp.Common;

namespace AlphaPointPayment.Web.Models.TrustPay
{
    public class PrePaymentInfoViewModel
    {
        public decimal Amount { get; set; }

        public Enums.Currencies Currency { get; set; }
    }
}