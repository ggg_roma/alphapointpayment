﻿using System.Security.Principal;
using BaseApp.Web.Models;

namespace BaseApp.Web.Code.Extensions
{
    public static class PrincipalExtensions
    {
        public static LoggedUserInfo GetUserInfo(this IPrincipal principal)
        {
            if(principal.Identity.IsAuthenticated)
            {
                return ((UserPrincipal) principal).UserInfo;
            }
            return null;
        }
    }
}
