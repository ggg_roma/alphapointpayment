﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Models;

namespace BaseApp.Web.Code.Menu
{
    public abstract class MenuBuilderBase
    {
        protected UrlHelper UrlHelper { get; private set; }
        protected UnitOfWork UnitOfWork { get; private set; }
        protected LoggedUserInfo LoggedUserInfo { get; private set; }

        protected MenuBuilderBase(UrlHelper urlHelper, LoggedUserInfo loggedUserInfo, UnitOfWork unitOfWork = null)
        {
            LoggedUserInfo = loggedUserInfo;
            UrlHelper = urlHelper;
            UnitOfWork = unitOfWork ?? DependencyResolver.Current.GetService<IUnitOfWorkFactoryPerRequest>().UnitOfWork;
        }

        protected abstract List<MenuItem> GetMenuItems();

        public MenuTree Build(bool hideItemsWithoutPermission = true, bool hideEmptyItems = true)
        {
            var menuItems = GetMenuItems();
          
            if (hideItemsWithoutPermission)
            {
                RemoveItemsWithoutPermission(menuItems, hideEmptyItems);
            }

            return new MenuTree
            {
                Items = menuItems
            };
        }

        private void RemoveItemsWithoutPermission(List<MenuItem> items, bool hideEmptyItems)
        {
            foreach (var menuItem in items.ToList())
            {
                if (!menuItem.HasPermission(UrlHelper))
                {
                    items.Remove(menuItem);
                }
                else if (menuItem.HasItems)
                {
                    RemoveItemsWithoutPermission(menuItem.Items, hideEmptyItems);
                    if (hideEmptyItems && !menuItem.HasItems && menuItem.UrlInfo == null)
                    {
                        items.Remove(menuItem);
                    }
                }
            }
        }
    }
}