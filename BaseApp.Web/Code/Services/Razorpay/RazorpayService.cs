﻿using AlphaPointPayment.API;
using AlphaPointPayment.API.Models.ResponseModels;
using AlphaPointPayment.Data.DataContext.Entities;
using BaseApp.Common;
using BaseApp.Data.Infrastructure;
using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlphaPointPayment.Web.Code.Services.Razorpay
{
    /// <summary>
    /// Razorpay payment service
    /// </summary>
    public class RazorpayService
    {
        private string key_id { get; set; }
        private string key_secret { get; set; }
        private string alphaPointAPIAddress { get; set; }

        #region ctor
        public RazorpayService()
        {
            key_id = ConfigSettings.RazorPayKey_id;
            key_secret = ConfigSettings.RazorPayKey_secret;
            alphaPointAPIAddress = ConfigSettings.AlphaPointAPI_Url;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Capture payment in Razorpay system
        /// </summary>
        /// <param name="paymentId">Payment identifier</param>
        /// <returns></returns>
        public Payment CapturePayment(string paymentId)
        {
            try
            {
                Payment paymentCaptured = RazorpayCapturePayment(paymentId);
                return paymentCaptured;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Create AlphaPoint Deposit Ticket in using AlphaPoint API
        /// </summary>
        /// <param name="accountID">Customer account ID</param>
        /// <param name="productID">Deposit product ID</param>
        /// <param name="Amount">Amount of the deposit</param>
        /// <returns></returns>
        public bool AlphaPointAPICall(int accountID, int productID, decimal Amount)
        {
            //TODO change it after testing
            AlphaPointCreateDepositTicketResponseModel alphapointAnswer = new AlphaPointPublicAPI(ConfigSettings.AlphaPointAPI_Test_Url).CreateDepositTicket(accountID, productID, Amount);
            //return alphapointAnswer.success;


            return true;
        }
        #endregion

        #region Private methods
        private Payment RazorpayCapturePayment(string razorpay_payment_id)
        {
            RazorpayClient client = new RazorpayClient(key_id, key_secret);
            Payment payment = client.Payment.Fetch(razorpay_payment_id);

            Dictionary<string, object> options = new Dictionary<string, object>();
            options.Add("amount", payment.Attributes["amount"]);

            return payment.Capture(options);
        }

        private void MapRazorpayCardFromPayment(Payment RazorPayment, ref RazorpayCard card)
        {
            try
            {
                var extractedCard = RazorPayment["card"];
                card.RazorpayCardId = extractedCard["id"];
                card.Entity = extractedCard["entity"];
                card.Name = extractedCard["name"];
                card.Last4 = extractedCard["last4"];
                card.Network = extractedCard["network"];
                card.Type = extractedCard["type"];
                card.Issuer = extractedCard["issuer"];
                card.International = extractedCard["international"];
                card.Emi = extractedCard["emi"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void MapPaymentToRazorpayPayment(Payment p, ref RazorpayPayment result)
        {
            try
            {
                result.RazorpayPaymentId = p.Attributes["id"];
                result.Entity = p.Attributes["entity"];
                result.Amount = p.Attributes["amount"];
                result.Currency = p.Attributes["currency"];
                result.Status = p.Attributes["status"];
                result.OrderId = p.Attributes["order_id"];
                result.InvoiceId = p.Attributes["invoice_id"];
                result.International = p.Attributes["amount"];
                result.Method = p.Attributes["method"];
                result.AmountRefunded = p.Attributes["amount_refunded"];
                result.RefundStatus = p.Attributes["refund_status"];
                result.Captured = p.Attributes["captured"];
                result.Description = p.Attributes["description"];
                result.CardId = p.Attributes["card_id"];
                result.Bank = p.Attributes["bank"];
                result.Wallet = p.Attributes["wallet"];
                result.VPA = p.Attributes["vpa"];
                result.Email = p.Attributes["email"];
                result.Contact = p.Attributes["contact"];
                result.CreateDate = DateTime.Now;
                result.Fee = p.Attributes["fee"] == null ? 0 : p.Attributes["fee"];
                result.Tax = p.Attributes["tax"] == null ? 0 : p.Attributes["tax"];
                result.ErrorCode = p.Attributes["error_code"];
                result.ErrorDescription = p.Attributes["error_description"];
                result.CreatedAt = p.Attributes["created_at"];
                result.ResponseObject = p.Attributes.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}