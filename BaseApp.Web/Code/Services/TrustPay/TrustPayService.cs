﻿using AlphaPointPayment.Web.Code.Services.TrustPay.Dto;
using BaseApp.Common;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AlphaPointPayment.Web.Code.Services.TrustPay
{
    public class TrustPayService : ServiceBase
    {
        public string _userId { get; set; }
        public string _password { get; set; }
        public string _entityId { get; set; }


        public TrustPayService(string baseUrl)
            : base(baseUrl)
        {
            _userId = ConfigSettings.TrustPayUserId;
            _password = ConfigSettings.TrustPayPassword;
            _entityId = ConfigSettings.TrustPayEntityId;
        }

        public GetPaymentDataRezult PrepareCheckout(decimal amount, Enums.Currencies currency)
        {
            Dictionary<string, dynamic> responseData;
            string data = $"authentication.userId={_userId}" +
                $"&authentication.password={_password}" +
                $"&authentication.entityId={_entityId}" +
                $"&amount={amount}" +
                $"&currency={currency}" +
                "&paymentType=DB";
            string url = $"{ConfigSettings.TrustPayBaseUrl}v1/checkouts";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            Stream PostData = request.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                var s = new JavaScriptSerializer();
                var parsed = JsonConvert.DeserializeObject<GetPaymentDataRezult>(reader.ReadToEnd(), new JsonSerializerSettings());
                reader.Close();
                dataStream.Close();

                return parsed;
            }
        }

        public async Task<GetPaymentDataRezult> GetPaymentDataAsync(string resourcePath)
        {
            string auth = $"authentication.userId={_userId}" +
               $"&authentication.password={_password}" +
               $"&authentication.entityId={_entityId}";
            string url = $"{resourcePath}?" + auth;
            var data = await PerformGetRequestAsync<GetPaymentDataRezult>(url);
            return data;
        }
    }
}