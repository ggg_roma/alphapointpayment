﻿
namespace AlphaPointPayment.Web.Code.Services.TrustPay.Dto
{
    public class GetPaymentDataRezult
    {
        public ResultDto result { get; set; }
        public string buildNumber { get; set; }
        public string timestamp { get; set; }
        public string ndc { get; set; }
        public string id { get; set; }
        public string type { get; set; }
    }
}