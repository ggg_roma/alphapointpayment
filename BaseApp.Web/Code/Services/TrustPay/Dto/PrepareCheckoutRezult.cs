﻿
namespace AlphaPointPayment.Web.Code.Services.TrustPay.Dto
{
    public class PrepareCheckoutRezult
    {
        public ResultDto result { get; set; }
        public string buildNumber { get; set; }
        public string timestamp { get; set; }
        public string ndc { get; set; }
        public string id { get; set; }
    }
}