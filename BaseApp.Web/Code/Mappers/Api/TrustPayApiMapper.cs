﻿using AlphaPointPayment.Data.DataContext.Entities;
using AlphaPointPayment.Web.Models.Api.TrustPay;
using BaseApp.Web.Code.Extensions;
using BaseApp.Web.Code.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlphaPointPayment.Web.Code.Mappers.Api
{
    public class TrustPayApiMapper : MapperBase
    {
        protected override void CreateMaps()
        {
            CreateMap<CreateNewPaymentInput, TrustPayPayment>()
                .Map(x => x.ModifiedUtc, x => DateTime.UtcNow)
                .Ignore(x => x.Id)
                .Ignore(x => x.CreationTime)
                .Ignore(x => x.PaymentStatus)
                .Ignore(x => x.SubmittedTime)       
                .Ignore(x => x.TrustPayApiResultDescription)
                .IgnoreAllUnmappedComplexTypes();

            CreateMap<TrustPayPayment, TrustPayPaymentDto>();
        }
    }
}