﻿using System;
using System.IO;
using System.Text;
using System.Web.Hosting;
using System.Web.Optimization;
using Yahoo.Yui.Compressor;

namespace BaseApp.Web.Code.Infrastructure.Bundling
{
    public class CssTransformOptions
    {
        public bool EnableMinification { get; set; }
    }

    public class CssTransform : IBundleTransform
    {
        private readonly CssTransformOptions options;

        public CssTransform(CssTransformOptions options)
        {
            if (options == null)
                throw new ArgumentNullException("options");

            this.options = options;
        }

        public void Process(BundleContext context, BundleResponse response)
        {
            var content = new StringBuilder();
            foreach (var f in response.Files)
            {
                string fileContent = File.ReadAllText(HostingEnvironment.MapPath(f.VirtualFile.VirtualPath));
                content.AppendLine(fileContent);
            }

            if (options.EnableMinification)
            {
                var compressor = new CssCompressor();
                response.Content = compressor.Compress(content.ToString());
            }
            else
            {
                response.Content = content.ToString();
            }

            response.ContentType = "text/css";
        }
    }
}