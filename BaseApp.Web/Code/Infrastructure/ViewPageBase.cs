﻿using System;
using System.Web.Mvc;
using BaseApp.Common;
using BaseApp.Data.Infrastructure;

namespace BaseApp.Web.Code.Infrastructure
{
    public abstract class ViewPageBase<T> : System.Web.Mvc.WebViewPage<T>
    {
        protected ViewDataItems DataItems { get; private set; } 

        protected ViewPageBase()
        {
            DataItems = new ViewDataItems(DependencyResolver.Current.GetService<IUnitOfWorkFactoryPerRequest>().UnitOfWork);
        }

        protected void SetCurrentMenuItem(Enums.MenuItemTypes menuItemType)
        {
            ViewBag.CurrentMenuItem = menuItemType;
        }

        protected Enums.MenuItemTypes? GetCurrentMenuItem()
        {
            return (Enums.MenuItemTypes?)ViewBag.CurrentMenuItem;
        }

    }
}