﻿using System;

namespace BaseApp.Web.Code.Infrastructure
{
    public class ApiEntityNotFoundException : ApiException
    {
        public enum Entity
        {
            User
        }

        public ApiEntityNotFoundException(Entity entity, object entityId, Exception innerException = null)
            : base(ApiResult.ApiResultCodes.EntityNotFound, string.Format("{0} not found. ID - {1}", entity, entityId), innerException)
        {
        }
    }
}