﻿using BaseApp.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace AlphaPointPayment.Web.Code.Infrastructure.BaseControllers.Api
{
    public abstract class ApiControllerBaseNoAuthorize : ApiController
    {
        private UnitOfWork _UnitOfWork;
        protected UnitOfWork UnitOfWork
        {
            get
            {
                if (_UnitOfWork == null)
                {
                    _UnitOfWork = DependencyResolver.Current.GetService<IUnitOfWorkFactoryPerRequest>().UnitOfWork;
                }
                return _UnitOfWork;
            }
        }
    }
}