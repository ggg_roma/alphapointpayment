﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Text;
using System.Web;

namespace BaseApp.Web.Code.Infrastructure
{
    public class IpAddressResolver
    {
        private readonly HttpContextBase HttpContext;
        private readonly HttpRequestMessage HttpRequestMessage;

        public IpAddressResolver(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");

            HttpContext = httpContext;
        }

        public IpAddressResolver(HttpRequestMessage httpRequestMessage)
        {
            if (httpRequestMessage == null)
                throw new ArgumentNullException("httpRequestMessage");

            HttpRequestMessage = httpRequestMessage;
        }

        public string GetUserHostIp()
        {
            if (HttpContext != null)
                return GetMvcClientIp();
            if (HttpRequestMessage != null)
                return GetWebApiClientIp();
            return null;
        }

        private string GetMvcClientIp()
        {
            string userHostIp = "";
            try
            {
                userHostIp = HttpContext.Request.Headers["X-Forwarded-For"];
                if (String.IsNullOrEmpty(userHostIp))
                {
                    //This is needed to avoid IPv6 IPs, as they may be returned by browser (for example in Windows 7 with IIS 7)
                    foreach (var ipa in System.Net.Dns.GetHostAddresses(HttpContext.Request.UserHostAddress))
                    {
                        if (ipa.AddressFamily.ToString() == "InterNetwork")
                        {
                            userHostIp = ipa.ToString();
                            break;
                        }
                    }

                    if (String.IsNullOrEmpty(userHostIp))
                    {
                        //Looks like we are on localhost
                        foreach (var ipa in System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName()))
                        {
                            if (ipa.AddressFamily.ToString() == "InterNetwork")
                            {
                                userHostIp = ipa.ToString();
                                break;
                            }
                        }
                    }
                }
            }
            catch { }

            return userHostIp;
        }

        private string GetWebApiClientIp()
        {
            if (HttpRequestMessage.Properties.ContainsKey("MS_HttpContext"))
            {
                var httpContext = (HttpContextWrapper)HttpRequestMessage.Properties["MS_HttpContext"];
                return new IpAddressResolver(httpContext).GetUserHostIp();
            }
            else if (HttpRequestMessage.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            {
                RemoteEndpointMessageProperty prop;
                prop = (RemoteEndpointMessageProperty)HttpRequestMessage.Properties[RemoteEndpointMessageProperty.Name];
                return prop.Address;
            }
            else
            {
                return null;
            }
        }
    }

}