﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using BaseApp.Web.Code.Infrastructure.BaseControllers;
using BaseApp.Web.Models.Example;

namespace BaseApp.Web.Controllers
{
    public class ExampleController : ControllerBaseAuthorizeRequired
    {
        //
        // GET: /Example/
        public ActionResult Countries()
        {
            return View(new CountryArgsModel());
        }

        public ActionResult GetCountriesList(CountryArgsModel args)
        {
            var countries = Mapper.Map<List<CountryListItemModel>>(
                UnitOfWork.Countries.GetCountries(args.Search, args.PagingSortingInfo)
            );

            return PartialView(new CountryListModel(args, countries));
        }
	}
}