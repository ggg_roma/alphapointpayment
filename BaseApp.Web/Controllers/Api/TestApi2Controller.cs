﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Code.Filters;
using BaseApp.Web.Code.Infrastructure;
using BaseApp.Web.Models.Api;

namespace BaseApp.Web.Controllers
{
    [BasicHttpAuthorize]
    public class TestApi2Controller : ApiController
    {
        [HttpPost]
        public ApiResult SaveTestFile(SaveTestFileArgs args)
        {
            return ApiResult.Success();
        }

        [HttpPost]
        public ApiResult SaveTest(SaveTestArgs args)
        {
            return ApiResult.Success();
        }
    }
}