﻿using AlphaPointPayment.API;
using AlphaPointPayment.Data.DataContext.Entities;
using AlphaPointPayment.Web.Code.Infrastructure.BaseControllers.Api;
using AlphaPointPayment.Web.Code.Services.Razorpay;
using AlphaPointPayment.Web.Models.Razorpay;
using BaseApp.Common;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Code.Infrastructure;
using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AlphaPointPayment.Web.Controllers.Api
{
    /// <summary>
    /// Controller for accepting payments using Razorpay
    /// </summary>
    public class RazorpayApiController : ApiControllerBaseNoAuthorize
    {
        private string key_id { get; set; }
        private string key_secret { get; set; }
        private string alphaPointAPIAddress { get; set; }

        private readonly RazorpayService service;

        #region ctor
        /// <summary>
        /// Default constructor
        /// </summary>
        public RazorpayApiController()
        {
            key_id = ConfigSettings.RazorPayKey_id;
            key_secret = ConfigSettings.RazorPayKey_secret;
            alphaPointAPIAddress = ConfigSettings.AlphaPointAPI_Url;

            service = new RazorpayService();
        }
        #endregion

        /// <summary>
        /// Verifies the purchase as complete by the merchant
        /// </summary>
        /// <param name="input">Identifier of the payment</param>
        /// <returns>Code and message</returns>
        [System.Web.Http.HttpPost]
        public ApiResult CapturePayment([FromBody] RazorpayResponse input)
        {
            string razorpay_payment_id = input.razorpay_payment_id;
            if (String.IsNullOrEmpty(razorpay_payment_id))
            {
                return ApiResult.Error(ApiResult.ApiResultCodes.Exception, "System had get bad response from Razorpay payment system. Please contact administrator.");
            }

            Payment paymentCaptured = service.CapturePayment(razorpay_payment_id);
            
            //Create new payment in database
            bool paymentCreated = CreatePayment(paymentCaptured);
            if (!paymentCreated)
            {
                return ApiResult.Error(ApiResult.ApiResultCodes.Exception, "The system unable to create payment. Please contact site administrator.");
            }

            //Create deposit ticket using AlphaPoint API
            AlphaPointPublicAPI aPI = new AlphaPointPublicAPI(alphaPointAPIAddress);

            //TODO Where we may get this parameters
            //now all this parameters set to random value
            int accountID = 1;
            int productID = 1;
            bool success = service.AlphaPointAPICall(accountID, productID, (decimal)paymentCaptured.Attributes["amount"]);

            //Send response to user
            if(!success)
            {
                return ApiResult.Error(ApiResult.ApiResultCodes.Exception, "The system unable to create AlphaPoint deposit ticket. Please contact site administrator.");
            }
            return ApiResult.Success();
        }

        /// <summary>
        /// Used for retrieving list of payments
        /// </summary>
        /// <returns>Serialized string in error message</returns>
        public ApiResult GetPayments()
        {
            RazorpayClient client = new RazorpayClient(key_id, key_secret);
            Dictionary<string, object> options = new Dictionary<string, object>();

            //supported option filters (from, to, count, skip)
            options.Add("count", 100);
            //options.Add("skip", 1000);

            List<Payment> result = client.Payment.All(options); var a = client.Payment.All();
            var res = ApiResult.Success();
            List<RazorpayPayment> mappedPayments = new List<RazorpayPayment>();
            foreach (var item in result)
            {
                RazorpayPayment p = new RazorpayPayment();
                MapPaymentToRazorpayPayment(item, ref p);
                mappedPayments.Add(p);
            }

            res.ErrorMessage = new JavaScriptSerializer().Serialize(mappedPayments);

            return res;
        }

        #region private methods

        private bool CreatePayment(Payment RazorPayment)
        {
            try
            {
                using (var tran = UnitOfWork.BeginTransaction())
                {
                    //create payment
                    var razorPayment = UnitOfWork.RazorpayPayments.CreateEmpty();
                    MapPaymentToRazorpayPayment(RazorPayment, ref razorPayment);

                    //create card
                    var card = UnitOfWork.RazorpayCards.CreateEmpty();
                    MapRazorpayCardFromPayment(RazorPayment, ref card);

                    UnitOfWork.SaveChanges();
                    tran.Commit();
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        private void MapRazorpayCardFromPayment(Payment RazorPayment, ref RazorpayCard card)
        {
            try
            {
                var extractedCard = RazorPayment["card"];
                card.RazorpayCardId = extractedCard["id"];
                card.Entity = extractedCard["entity"];
                card.Name = extractedCard["name"];
                card.Last4 = extractedCard["last4"];
                card.Network = extractedCard["network"];
                card.Type = extractedCard["type"];
                card.Issuer = extractedCard["issuer"];
                card.International = extractedCard["international"];
                card.Emi = extractedCard["emi"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void MapPaymentToRazorpayPayment(Payment p, ref RazorpayPayment result)
        {
            try
            {
                result.RazorpayPaymentId = p.Attributes["id"];
                result.Entity = p.Attributes["entity"];
                result.Amount = p.Attributes["amount"];
                result.Currency = p.Attributes["currency"];
                result.Status = p.Attributes["status"];
                result.OrderId = p.Attributes["order_id"];
                result.InvoiceId = p.Attributes["invoice_id"];
                result.International = p.Attributes["amount"];
                result.Method = p.Attributes["method"];
                result.AmountRefunded = p.Attributes["amount_refunded"];
                result.RefundStatus = p.Attributes["refund_status"];
                result.Captured = p.Attributes["captured"];
                result.Description = p.Attributes["description"];
                result.CardId = p.Attributes["card_id"];
                result.Bank = p.Attributes["bank"];
                result.Wallet = p.Attributes["wallet"];
                result.VPA = p.Attributes["vpa"];
                result.Email = p.Attributes["email"];
                result.Contact = p.Attributes["contact"];
                result.CreateDate = DateTime.Now;
                result.Fee = p.Attributes["fee"] == null ? 0 : p.Attributes["fee"];
                result.Tax = p.Attributes["tax"] == null ? 0 : p.Attributes["tax"];
                result.ErrorCode = p.Attributes["error_code"];
                result.ErrorDescription = p.Attributes["error_description"];
                result.CreatedAt = p.Attributes["created_at"];
                result.ResponseObject = p.Attributes.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
