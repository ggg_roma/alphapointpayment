﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Code.Filters;
using BaseApp.Web.Code.Infrastructure;
using BaseApp.Web.Models.Api;
using AlphaPointPayment.Web.Models.Api.TrustPay;
using AutoMapper;
using AlphaPointPayment.Data.DataContext.Entities;
using BaseApp.Common;
using AlphaPointPayment.Web.Code.Infrastructure.BaseControllers.Api;
using AlphaPointPayment.Web.Code.Services.TrustPay;
using AlphaPointPayment.API;

namespace BaseApp.Web.Controllers
{
    public class TrustPayApiController : ApiControllerBaseNoAuthorize
    {
        /// <summary>
        /// Checkout preparing in TrustPay api
        /// </summary>
        [HttpPost]
        public ApiResult<PrepareCheckoutOutput> PrepareCheckout(PrepareCheckoutInput input)
        {
            var trustPayService = new TrustPayService(ConfigSettings.TrustPayBaseUrl);
            var data = trustPayService.PrepareCheckout(input.Amount, input.Currency);
            //if (data.result.code != "000.200.100")
            //{
            //    return ApiResult.Error<PrepareCheckoutOutput>(ApiResult.ApiResultCodes.Exception, data.result.description);
            //}
            var model = new PrepareCheckoutOutput(data.id, data.result.description);

            return ApiResult.Success(model);
        }

        /// <summary>
        /// New payment creation in database before sending payment to TrustPay
        /// </summary>
        [HttpPost]
        public ApiResult<TrustPayPaymentDto> CreateNewPayment(CreateNewPaymentInput input)
        {
            var newTrustPayPayment = UnitOfWork.TrustPayPayments.CreateEmpty();
            var dbPayment = Mapper.Map(input, newTrustPayPayment);
            dbPayment.CreationTime = DateTime.UtcNow;
            dbPayment.PaymentStatus = Enums.PaymentStatuses.New;

            UnitOfWork.SaveChanges();
            var paymentDto = Mapper.Map<TrustPayPaymentDto>(dbPayment);

            return ApiResult.Success(paymentDto);
        }

        /// <summary>
        /// To use after trust pay payment proceed
        /// </summary>
        [HttpGet]
        public async Task<ApiResult<CheckTrustPayPaymentOutput>> CheckTrustPayPayment([FromUri] CheckTrustPayPaymentInput input)
        {
            var trustPayService = new TrustPayService(ConfigSettings.TrustPayBaseUrl);
            var data = await trustPayService.GetPaymentDataAsync(input.resourcePath);                  
            var dbPayment = UnitOfWork.TrustPayPayments.GetByCheckoutIdOrNull(input.checkoutId);
            if (dbPayment == null)
            {
                return ApiResult.Error<CheckTrustPayPaymentOutput>(ApiResult.ApiResultCodes.Exception, $"No record with checkout id {input.checkoutId} in db!");
            }
            dbPayment.TrustPayApiResultDescription = $"{data.result.code} : {data.result.description}" ;
            if(!data.result.code.StartsWith("000.000"))
            {
                dbPayment.PaymentStatus = Enums.PaymentStatuses.Error;
                dbPayment.ModifiedUtc = DateTime.UtcNow;
                UnitOfWork.SaveChanges();
                return ApiResult.Error<CheckTrustPayPaymentOutput>(ApiResult.ApiResultCodes.Exception, $"{data.result.code} : {data.result.description}");
            }
            if (dbPayment.CheckoutId != data.ndc)
            {
                dbPayment.PaymentStatus = Enums.PaymentStatuses.WrongComparedData;
                dbPayment.ModifiedUtc = DateTime.UtcNow;
                UnitOfWork.SaveChanges();
                return ApiResult.Error<CheckTrustPayPaymentOutput>(ApiResult.ApiResultCodes.Exception, "Wrong compared data, please try again!");
            }

            using (var tran = UnitOfWork.BeginTransaction())
            {
                var apApi = new AlphaPointPublicAPI(ConfigSettings.AlphaPointAPI_Test_Url);
                //ToDo: use CreateDeposit ticket after implementing
                //apApi.GetProducts();
                dbPayment.PaymentStatus = Enums.PaymentStatuses.Submitted;
                dbPayment.SubmittedTime = DateTime.UtcNow;
                dbPayment.ModifiedUtc = DateTime.UtcNow;
                UnitOfWork.SaveChanges();
                tran.Commit();
            }

            return ApiResult.Success(new CheckTrustPayPaymentOutput { Message = "Payment successfull!" });
        }
    }
}