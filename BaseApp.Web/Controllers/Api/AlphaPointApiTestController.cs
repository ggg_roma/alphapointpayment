﻿using AlphaPointPayment.API;
using AlphaPointPayment.API.Models.ResponseModels;
using AlphaPointPayment.Web.Code.Infrastructure.BaseControllers.Api;
using BaseApp.Common;
using BaseApp.Web.Code.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AlphaPointPayment.Web.Controllers.Api
{
    /// <summary>
    /// Controller for testing AlphaPoint API Methods
    /// Controller USED FOR TEST ONLY
    /// </summary>
    public class AlphaPointApiTestController : ApiControllerBaseNoAuthorize
    {
        private string AP_user;
        private string AP_pass;
        private string API_link;

        //TODO Remove after test
        //41 is Account Id from WebAuthenticateUser(AP_user, AP_pass)
        private int UserAccountID = 41;

        public AlphaPointApiTestController()
        {
            AP_user = ConfigSettings.AlphaPointAPI_UserName;
            AP_pass = ConfigSettings.AlphaPointAPI_UserPassword;
            API_link = ConfigSettings.AlphaPointAPI_Test_Url;
        }

        [HttpGet]
        public ApiResult Logout()
        {
            var res = new AlphaPointPayment.API.AlphaPointPublicAPI(ConfigSettings.AlphaPointAPI_Test_Url).LogOut();

            if (res) return ApiResult.Success();

            return ApiResult.Error(ApiResult.ApiResultCodes.Exception, "cannot logout");
        }

        [HttpGet]
        public ApiResult<string> GetOMSs()
        {
            var res = new AlphaPointPayment.API.AlphaPointPublicAPI(ConfigSettings.AlphaPointAPI_Test_Url).GetOMSs(1);

            string resStr = "";
            foreach (var item in res)
            {
                resStr += "[ OMSId:" + item.OMSId + ", OMSName:" + item.OMSName + "] ";
            }

            if (res.Count > 0) return ApiResult<string>.Success(resStr);

            return ApiResult<string>.Success("There is no OMS's");
        }

        [HttpGet]
        public ApiResult WebAuthenticateUser()
        {
            AlphaPointPublicAPI api = new AlphaPointPublicAPI(API_link);
            var res = api.WebAuthenticateUser(AP_user, AP_pass);
            if (res.Authenticated) return ApiResult.Success();
            return ApiResult.Error(ApiResult.ApiResultCodes.EntityNotFound, "There is nno user with such username and password.");
        }

        [HttpGet]
        public ApiResult AuthenticateUserNamePass()
        {
            AlphaPointPublicAPI api = new AlphaPointPublicAPI(API_link);
            var res = api.AuthenticateUser(AP_user, AP_pass);
            if (res.Authenticated) return ApiResult.Success();
            return ApiResult.Error(ApiResult.ApiResultCodes.EntityNotFound, "There is nno user with such username and password.");
        }

        [HttpGet]
        [Obsolete]
        public ApiResult AuthenticateUserKeys()
        {
            var res = new AlphaPointPublicAPI(ConfigSettings.AlphaPointAPI_Test_Url).AuthenticateUser("qqqqqqqqqqq1", "wwwwwww", 1);
            if (res.Authenticated) return ApiResult.Success();
            return ApiResult.Error(ApiResult.ApiResultCodes.EntityNotFound, "There is nno user with such username and password.");
        }

        [HttpGet]
        public ApiResult<string> GetDepositTickets()
        {
            var service = new AlphaPointPublicAPI(API_link);
            var authRes = service.AuthenticateUser(AP_user, AP_pass);

            if (!authRes.Authenticated)
            {
                return ApiResult<string>.Error<string>(ApiResult.ApiResultCodes.Exception, "User is not authorized!!!");
            }

            var res = service.GetDepositTickets(authRes.User.AccountId);

            string resStr = "";
            foreach (var item in res)
            {
                resStr += new JavaScriptSerializer().Serialize(item) + Environment.NewLine;
            }

            if (res.Count > 0) return ApiResult<string>.Success(resStr);

            return ApiResult<string>.Success("There is no deposit tickets");
        }

        [HttpGet]
        public ApiResult<string> GetProducts()
        {
            var res = new AlphaPointPublicAPI(ConfigSettings.AlphaPointAPI_Test_Url).GetProducts();

            string resStr = "";
            foreach (var item in res)
            {
                resStr += new JavaScriptSerializer().Serialize(item) + Environment.NewLine;
            }

            if (res.Count > 0) return ApiResult<string>.Success(resStr);

            return ApiResult<string>.Success("There is no OMS's");
        }

        [HttpGet]
        public ApiResult<string> GetProduct(int? id)
        {
            var service = new AlphaPointPublicAPI(API_link);
            if (id == null)
            {
                return ApiResult.Error(ApiResult.ApiResultCodes.Exception ,"Invalid Id") as ApiResult<string>;
            }
            var res = service.GetProduct((int)id);

            return ApiResult<string>.Success(new JavaScriptSerializer().Serialize(res));
        }

        [HttpGet]
        public ApiResult<string> GetAccountInfo()
        {
            var service = new AlphaPointPublicAPI(API_link);
            var res = service.GetAccountInfo(40);
            var a = service.GetAllDepositTickets();

            return ApiResult<string>.Success(res);
        }

        [HttpGet]
        public ApiResult<string> GetAGetAllDepositTickets()
        {
            var service = new AlphaPointPublicAPI(API_link);
            var res = service.GetAllDepositTickets();

            return null;
        }



        [HttpGet]
        public ApiResult<string> CreateDepositTicket()
        {
            var service = new AlphaPointPublicAPI(API_link);
            var res = service.CreateDepositTicket(41, 1, 100);

            return ApiResult<string>.Success(new JavaScriptSerializer().Serialize(res));
        }

        [HttpGet]
        public ApiResult<string> GetDepositInfo(int productId)
        {
            var service = new AlphaPointPublicAPI(API_link);

            var res = service.GetDepositInfo(41, productId); 

            return ApiResult<string>.Success(new JavaScriptSerializer().Serialize(res));
        }



    }
}
