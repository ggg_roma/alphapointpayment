﻿using AlphaPointPayment.Web.Models.Razorpay;
using BaseApp.Common;
using BaseApp.Web.Code.Infrastructure.BaseControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlphaPointPayment.Web.Controllers
{
    /// <summary>
    /// Controller for demonstration
    /// </summary>
    public class RazorpayController : ControllerBaseNoAuthorize
    {

        // GET: Razorpay
        public ActionResult Index()
        {
            return View(new RazorpayInitModel());
        }

        /// <summary>
        /// Prepared data to razor pay checkout
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public ActionResult PrepareCheckout(RazorpayInitModel input)
        {
            input.PublicKey = ConfigSettings.RazorPayKey_id;
            input.LogoUrl = "https://alphapoint.com/assets/img/default_logo.svg";
            input.Method = "card";

            return View(input);
        }

        public ActionResult TestPsiGate()
        {
            return View();
        }
    }
}