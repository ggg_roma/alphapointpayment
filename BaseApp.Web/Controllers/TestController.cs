﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.WebPages;
using BaseApp.Data.DataContext;
using BaseApp.Web.Code.Filters;
using BaseApp.Web.Code.Infrastructure;
using BaseApp.Web.Code.Infrastructure.BaseControllers;

namespace BaseApp.Web.Controllers
{
    public class TestController : ControllerBaseNoAuthorize
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            //var c = UnitOfWork.Countries.GetCountries2().Select(m => m.Name);

            //return Json(c, JsonRequestBehavior.AllowGet);

            ClientMessage.AddError("TEST ERROR REQUEST");
            ClientMessage.AddSuccess("<p style='color:red;'>TEST SUCCESS REQUEST</p>");
           

            return View();
        }

        [HttpPost]
        public ActionResult Index(int? Country, int? State)
        {
            return View();
        }

        [Authorize]
        public ActionResult TestAjaxRedirect()
        {
            return RedirectToAction("Index", "Home");
        }

        /*public ActionResult TestError()
        {
            var user = UnitOfWork.Users.GetAll().First();
            //UnitOfWork.Users.MarkForDelete(user);
            user.Login = "test sadasd adad affffffffffffffffffffffffffffffff test sadasd adad affffffffffffffffffffffffffffffff test sadasd adad affffffffffffffffffffffffffffffff";

            try
            {
                UnitOfWork.SaveChanges();
            }
            catch (DbUpdateExceptionWrapper ex)
            {
                throw;
            }

            throw new Exception("test");
        }*/

        public void TestAjaxMesssages()
        {
            ClientMessage.AddSuccess("<p style='color: green;'>GREEN TEST SUCCESS</p>");
            ClientMessage.AddError("<p style='color: red;'>RED TEST SUCCESS</p>");
            ClientMessage.AddJavaScript("alert(111);");
        }

        public ActionResult TestMsg()
        {
            ClientMessage.AddSuccess("TEST SUCCESS REQUEST");
            return RedirectToAction("Index");
        }

        public void TestPart()
        {
            var resp = this.Response;

            resp.BufferOutput = false;
            resp.Buffer = false;

            resp.Write("1");
            resp.Flush();
            System.Threading.Thread.Sleep(3000);

            resp.Write("2");
            resp.Flush();
            System.Threading.Thread.Sleep(3000);

            resp.Write("3");
            resp.Flush();
            System.Threading.Thread.Sleep(3000);

        }

        /*public ActionResult TestTransaction()
        {
            var newUser = new User()
                          {
                              Login = "TEST",
                              Email = "TEST@test.com",
                              FirstName = "TEST",
                              LastName = "TEST",
                              Password = "1",
                              CreatedDate = DateTime.Now,
                              UpdatedDate = DateTime.Now
                          };

            UnitOfWork.Users.MarkForInsert(newUser);

            using (var tran = UnitOfWork.BeginTransaction())
            {
                UnitOfWork.SaveChanges();

                var userId = newUser.Id;

                try
                {
                    using (var innerTran = UnitOfWork.BeginTransaction())
                    {
                        newUser.FirstName = "edited";

                        UnitOfWork.SaveChanges();

                        //throw new Exception("TEST");

                        innerTran.Commit();
                    }
                }
                catch (Exception)
                {
                    
                }
                

                tran.Commit();
            }

            var users = UnitOfWork.Users.GetAll();

            return Content("OK");
        }*/
    }
}