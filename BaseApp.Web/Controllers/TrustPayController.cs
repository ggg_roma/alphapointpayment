﻿using AlphaPointPayment.Web.Models.TrustPay;
using BaseApp.Web.Code.Infrastructure.BaseControllers;
using System.Web.Mvc;

namespace AlphaPointPayment.Web.Controllers
{
    public class TrustPayController : ControllerBaseNoAuthorize
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetPrePaymentInfo(PrePaymentInfoViewModel model)
        {
            return PartialView("_PrePaymentInfo", model);
        }

        [HttpGet]
        public ActionResult TrustPayRedirect([System.Web.Http.FromUri] TrustPayRedirectInput input)
        {
            return View(input);
        }
    }
}