﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BaseApp.Web.Code.Infrastructure.BaseControllers;
using Razorpay.Api;

namespace BaseApp.Web.Controllers
{
    public class HomeController : ControllerBaseNoAuthorize
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult NewApiTest()
        {
            //Payment setup example:
            string baseUrl = "https://playground.trustpay.eu/mapi5/Card/Pay";
            long AID = 2107111111;
            decimal AMT = 1.5M;
            string CUR = "EUR";
            string REF = "cae50266-4582-47cb-91a0-7fb00727241b_41";

            string secretKey = "123456";
            string sigData = string.Format(
                CultureInfo.InvariantCulture,
                "{0}{1:0.00}{2}{3}", AID, AMT, CUR, REF);
            string SIG = GetSignature(secretKey, sigData);

            string url = string.Format(
                CultureInfo.InvariantCulture,
                "{0}?AID={1}&AMT={2:0.00}&CUR={3}&REF={4}&SIG={5}&EURL=http://localhost:5180/Error",
                baseUrl, AID, AMT, CUR, HttpUtility.UrlEncode(REF), SIG);

            return Redirect(url);
        }

        public string GetSignature(string key, string message)
        {
            using (var hmac = HMAC.Create("HMACSHA256"))
            {
                hmac.Key = Encoding.UTF8.GetBytes(key);
                byte[] signature = hmac.ComputeHash(Encoding.UTF8.GetBytes(message));
                return BitConverter.ToString(signature).Replace("-", "").ToUpperInvariant();
            }
        }

    }
}
