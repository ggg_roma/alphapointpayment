﻿using AlphaPointPayment.Web.Code.Services.Polipayment;
using BaseApp.Common;
using BaseApp.Web.Code.Infrastructure.BaseControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlphaPointPayment.Web.Controllers
{
    public class PolipaymentController : ControllerBaseNoAuthorize
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}