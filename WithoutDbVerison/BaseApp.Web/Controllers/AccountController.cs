﻿//using System;
//using System.Collections.Generic;
//using System.Web.Mvc;
//using System.Web.Security;
//using BaseApp.Common.Utils;
//using BaseApp.Data.DataContext.Entities;
//using BaseApp.Web.Code.Extensions;
//using BaseApp.Web.Code.Infrastructure;
//using BaseApp.Web.Code.Infrastructure.BaseControllers;
//using BaseApp.Web.Code.Infrastructure.Templating;
//using BaseApp.Web.Models.Account;
//using BaseApp.Web.Models.TemplateModels;
//using AutoMapper;

//namespace BaseApp.Web.Controllers
//{
//    public class AccountController : ControllerBaseAuthorizeRequired
//    {
//        [AllowAnonymous]
//        public ActionResult LogOn()
//        {
//            if (User.GetUserInfo() != null)
//            {
//                return Redirect(Url.Home());
//            }

//            return View();
//        }

//        [HttpPost]
//        [AllowAnonymous]
//        public ActionResult LogOn(LogOnModel model, string returnUrl)
//        {
//            if (User.GetUserInfo() != null)
//            {
//                return Redirect(Url.Home());
//            }

//            if (ModelState.IsValid)
//            {
//                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);

//                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
//                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
//                {
//                    return Redirect(returnUrl);
//                }
//                else
//                {
//                    return Redirect(Url.Home());
//                }
//            }

//            // If we got this far, something failed, redisplay form
//            return View(model);
//        }

//        public ActionResult LogOff()
//        {
//            LogonManager.SignOut(HttpContext);

//            return Redirect(Url.Home());
//        }

//        [HttpGet]
//        public ActionResult UserProfile()
//        {
//            User user = UnitOfWork.Users.GetIncludeRolesOrNull(User.GetUserInfo().UserID);
//            var model = Mapper.Map<UserProfileModel>(user);

//            return View(model);
//        }

//        [HttpPost]
//        public ActionResult UserProfile(UserProfileModel model)
//        {
//            if (ModelState.IsValid)
//            {
//                User user = UnitOfWork.Users.GetIncludeRolesOrNull(User.GetUserInfo().UserID);

//                Mapper.Map(model, user);
//                user.UpdatedByUserId = User.GetUserInfo().UserID;
//                user.UpdatedDate = DateTime.Now;

//                UnitOfWork.SaveChanges();
//                LogonManager.RefreshCurrentLoggedUserInfo(HttpContext, user.Login);

//                ClientMessage.AddSuccess("Profile was successfully updated.");

//                return RedirectToAction("UserProfile");
//            }

//            if (Request.IsAjaxRequest())
//            {
//                return PartialView("_Profile", model);
//            }

//            return View(model);
//        }

//        //
//        // GET: /Account/Register

//        [AllowAnonymous]
//        public ActionResult Register()
//        {
//            if (User.GetUserInfo() != null)
//            {
//                return Redirect(Url.Home());
//            }
//            return View();
//        }

//        //
//        // POST: /Account/Register

//        [HttpPost]
//        [AllowAnonymous]
//        public ActionResult Register(RegisterModel model)
//        {
//            if (User.GetUserInfo() != null)
//            {
//                return Redirect(Url.Home());
//            }

//            if (ModelState.IsValid)
//            {
//                var user = new User
//                {
//                    Login = model.UserName,
//                    Email = model.Email,
//                    FirstName = model.FirstName,
//                    LastName = model.LastName,
//                    Password = PasswordHash.HashPassword(model.Password),
//                    Roles = new List<Role>() { UnitOfWork.Users.GetUserRole() },
//                    CreatedDate = DateTime.Now,
//                    UpdatedDate = DateTime.Now

//                };
//                UnitOfWork.Users.MarkForInsert(user);
//                UnitOfWork.SaveChanges();

//                FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);

//                return Redirect(Url.Home());
//            }

//            // If we got this far, something failed, redisplay form
//            return View(model);
//        }

//        //
//        // GET: /Account/ChangePassword

//        public ActionResult ChangePassword()
//        {
//            return View();
//        }

//        //
//        // POST: /Account/ChangePassword

//        [HttpPost]
//        public ActionResult ChangePassword(ChangePasswordModel model)
//        {
//            if (ModelState.IsValid)
//            {
//                User user = UnitOfWork.Users.GetIncludeRolesOrNull(User.GetUserInfo().UserID);

//                user.Password = PasswordHash.HashPassword(model.NewPassword);
//                user.UpdatedDate = DateTime.Now;
//                user.UpdatedByUserId = User.GetUserInfo().UserID;

//                UnitOfWork.SaveChanges();

//                return RedirectToAction("ChangePasswordSuccess");
//            }

//            // If we got this far, something failed, redisplay form
//            return View(model);
//        }

//        public ActionResult _ChangePassword()
//        {
//            return View();
//        }

//        [HttpPost]
//        public ActionResult _ChangePassword(ChangePasswordModel model)
//        {
//            if (ModelState.IsValid)
//            {
//                var user = UnitOfWork.Users.GetIncludeRolesOrNull(User.GetUserInfo().UserID);

//                user.Password = PasswordHash.HashPassword(model.NewPassword);
//                user.UpdatedDate = DateTime.Now;
//                user.UpdatedByUserId = User.GetUserInfo().UserID;

//                UnitOfWork.SaveChanges();
                
//                return CloseDialog(new CloseDialogArgs() { SuccessMessage = "Your password has been changed successfully." });
//            }

//            // If we got this far, something failed, redisplay form
//            return View(model);
//        }
        
//        public ActionResult ChangePasswordSuccess()
//        {
//            return View();
//        }

//        [AllowAnonymous]
//        public ActionResult ForgotPassword()
//        {
//            return View(new ForgotPasswordModel());
//        }

//        [HttpPost]
//        [AllowAnonymous]
//        public ActionResult ForgotPassword(ForgotPasswordModel model)
//        {
//            if(ModelState.IsValid)
//            {
//                var user = UnitOfWork.Users.GetByEmailOrNull(model.Email);

//                var forgotPassword = new UserForgotPassword()
//                {
//                    CreatedDate = DateTime.Now,
//                    CreatorIpAddress = new IpAddressResolver(HttpContext).GetUserHostIp(),
//                    RequestGuid = Guid.NewGuid()
//                };
//                user.UserForgotPasswords.Add(forgotPassword);

//                string resetPasswordUrl = Url.Action("CompleteResetPassword", "Account",
//                                                     new { id = forgotPassword.RequestGuid },
//                                                     "http");

//                var emailModel = new ResetPasswordModel
//                {
//                    RequestIp = forgotPassword.CreatorIpAddress,
//                    ResetPasswordUrl = resetPasswordUrl,
//                    UserName = "user.FullName"
//                };
//                string emailBody = TemplateBuilder.Render("ResetPassword", emailModel);

//                UnitOfWork.SaveChanges();
//                EmailSender.SendEmail(new[] { new EmailAddressInfo(user.Email, "user.FullName") }, null, null, "Password reset confirmation", emailBody);

//                return RedirectToAction("ForgotPasswordSuccess");
//            }
//            return View(model);
//        }

//        [AllowAnonymous]
//        public ActionResult ForgotPasswordSuccess()
//        {
//            return View();
//        }

//        [AllowAnonymous]
//        public ActionResult CompleteResetPassword(Guid id)
//        {
//            var forgotPasswordRequest = UnitOfWork.Users.GetForgotPasswordRequest(id);
//            string error;
//            if (!TryValidateForgotPasswordRequest(forgotPasswordRequest, out error))
//            {
//                return View("CompleteResetPasswordError", (object)error);
//            }
//            return View(new CompleteResetPasswordModel());
//        }
        
//        [HttpPost]
//        [AllowAnonymous]
//        public ActionResult CompleteResetPassword(Guid id, CompleteResetPasswordModel model)
//        {
//            if(ModelState.IsValid)
//            {
//                var forgotPasswordRequest = UnitOfWork.Users.GetForgotPasswordRequest(id);
//                string error;
//                if (!TryValidateForgotPasswordRequest(forgotPasswordRequest, out error))
//                {
//                    return View("CompleteResetPasswordError", (object)error);
//                }

//                forgotPasswordRequest.ApprovedDateTime = DateTime.Now;
//                forgotPasswordRequest.ApproverIpAddress = new IpAddressResolver(HttpContext).GetUserHostIp();

//                forgotPasswordRequest.User.Password = PasswordHash.HashPassword(model.NewPassword);
//                forgotPasswordRequest.User.UpdatedDate = DateTime.Now;
//                forgotPasswordRequest.User.UpdatedByUserId = forgotPasswordRequest.User.Id;
                
//                UnitOfWork.SaveChanges();

//                return RedirectToAction("CompleteResetPasswordSuccess");
//            }
//            return View(model);
//        }

//        [AllowAnonymous]
//        public ActionResult CompleteResetPasswordSuccess()
//        {
//            return View();
//        }

//        private bool TryValidateForgotPasswordRequest(UserForgotPassword forgotPasswordRequest, out string error)
//        {
//            error = "";
//            if (forgotPasswordRequest == null)
//            {
//                error = "Reset Password key not found";
//            }
//            else if (forgotPasswordRequest.ApprovedDateTime != null || forgotPasswordRequest.IsExpired)
//            {
//                error = "Reset Password url expired";
//            }
//            return String.IsNullOrEmpty(error);
//        }
//    }
//}