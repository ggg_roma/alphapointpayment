﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BaseApp.Web.Code.Filters;
using BaseApp.Web.Code.Infrastructure;
using AlphaPointPayment.Web.Models.Api.TrustPay;
using AutoMapper;
using BaseApp.Common;
using AlphaPointPayment.Web.Code.Infrastructure.BaseControllers.Api;
using AlphaPointPayment.Web.Code.Services.TrustPay;
using AlphaPointPayment.API;

namespace BaseApp.Web.Controllers
{
    public class TrustPayApiController : ApiControllerBaseNoAuthorize
    {
        /// <summary>
        /// Checkout preparing in TrustPay api
        /// </summary>
        [HttpPost]
        public ApiResult<PrepareCheckoutOutput> PrepareCheckout(PrepareCheckoutInput input)
        {
            var trustPayService = new TrustPayService(ConfigSettings.TrustPayBaseUrl);
            var data = trustPayService.PrepareCheckout(input.Amount, input.Currency);
            //if (data.result.code != "000.200.100")
            //{
            //    return ApiResult.Error<PrepareCheckoutOutput>(ApiResult.ApiResultCodes.Exception, data.result.description);
            //}
            var model = new PrepareCheckoutOutput(data.id, data.result.description);

            return ApiResult.Success(model);
        }

        /// <summary>
        /// New payment creation in database before sending payment to TrustPay
        /// </summary>
        //[HttpPost]
        //public ApiResult<TrustPayPaymentDto> CreateNewPayment(CreateNewPaymentInput input)
        //{
        //    var newTrustPayPayment = UnitOfWork.TrustPayPayments.CreateEmpty();
        //    var dbPayment = Mapper.Map(input, newTrustPayPayment);
        //    dbPayment.CreationTime = DateTime.UtcNow;
        //    dbPayment.PaymentStatus = Enums.PaymentStatuses.New;

        //    UnitOfWork.SaveChanges();
        //    var paymentDto = Mapper.Map<TrustPayPaymentDto>(dbPayment);

        //    return ApiResult.Success(paymentDto);
        //}

        /// <summary>
        /// To use after trust pay payment proceed
        /// </summary>
        [HttpGet]
        public async Task<ApiResult<CheckTrustPayPaymentOutput>> CheckTrustPayPayment([FromUri] CheckTrustPayPaymentInput input)
        {
            var trustPayService = new TrustPayService(ConfigSettings.TrustPayBaseUrl);
            var data = await trustPayService.GetPaymentDataAsync(input.resourcePath);                  
            if(!data.result.code.StartsWith("000.000"))
            {
                return ApiResult.Error<CheckTrustPayPaymentOutput>(ApiResult.ApiResultCodes.Exception, $"Error : {data.result.code} - {data.result.description}");
            }
            if (input.checkoutId != data.ndc)
            {
                return ApiResult.Error<CheckTrustPayPaymentOutput>(ApiResult.ApiResultCodes.Exception, "Wrong compared data, please try again!");
            }

            var apApi = new AlphaPointPublicAPI();
            //ToDo: use UpdateDepositTicket after implementing
            //apApi.GetProducts();

            return ApiResult.Success(new CheckTrustPayPaymentOutput { Message = "Payment successfull!" });
        }
    }
}