﻿using AlphaPointPayment.API;
using AlphaPointPayment.API.Models.ResponseModels;
using AlphaPointPayment.Web.Code.Infrastructure.BaseControllers.Api;
using BaseApp.Common;
using BaseApp.Web.Code.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AlphaPointPayment.Web.Controllers.Api
{
    /// <summary>
    /// Controller for testing AlphaPoint API Methods
    /// Controller USED FOR TEST ONLY
    /// </summary>
    public class AlphaPointApiTestController : ApiControllerBaseNoAuthorize
    {
        private string AP_user;
        private string AP_pass;
        public AlphaPointApiTestController()
        {
            AP_user = ConfigSettings.AlphaPointAPI_UserName;
            AP_pass = ConfigSettings.AlphaPointAPI_UserPassword;
        }

        [HttpGet]
        public ApiResult Logout()
        {
            var res = new AlphaPointPayment.API.AlphaPointPublicAPI().LogOut();

            if (res) return ApiResult.Success();

            return ApiResult.Error(ApiResult.ApiResultCodes.Exception, "cannot logout");
        }

        [HttpGet]
        public ApiResult<string> GetOMSs()
        {
            var res = new AlphaPointPayment.API.AlphaPointPublicAPI().GetOMSs(1);

            string resStr = "";
            foreach (var item in res)
            {
                resStr += "[ OMSId:" + item.OMSId + ", OMSName:" + item.OMSName + "] ";
            }

            if (res.Count > 0) return ApiResult<string>.Success(resStr);

            return ApiResult<string>.Success("There is no OMS's");
        }

        [HttpGet]
        public ApiResult WebAuthenticateUser()
        {
            var res = new AlphaPointPublicAPI().WebAuthenticateUser(AP_user, AP_pass);
            if (res.Authenticated) return ApiResult.Success();
            return ApiResult.Error(ApiResult.ApiResultCodes.EntityNotFound, "There is nno user with such username and password.");
        }

        [HttpGet]
        public ApiResult AuthenticateUserNamePass()
        {
            var res = new AlphaPointPublicAPI().AuthenticateUser(AP_user, AP_pass);
            if (res.Authenticated) return ApiResult.Success();
            return ApiResult.Error(ApiResult.ApiResultCodes.EntityNotFound, "There is nno user with such username and password.");
        }

        [HttpGet]
        public ApiResult AuthenticateUserKeys()
        {
            var res = new AlphaPointPublicAPI().AuthenticateUser("qqqqqqqqqqq1", "wwwwwww", 1);
            if (res.Authenticated) return ApiResult.Success();
            return ApiResult.Error(ApiResult.ApiResultCodes.EntityNotFound, "There is nno user with such username and password.");
        }

    }
}
