﻿//using AlphaPointPayment.Web.Code.Infrastructure.BaseControllers.Api;
//using AlphaPointPayment.Web.Code.Services.Polipayment;
//using AlphaPointPayment.Web.Models.Api.Polipayment;
//using BaseApp.Common;
//using BaseApp.Web.Code.Infrastructure;
//using System.Web.Mvc;

//namespace AlphaPointPayment.Web.Controllers.Api
//{
//    public class PolipaymentApiController : ApiControllerBaseNoAuthorize
//    {
//        [HttpPost]
//        public ApiResult<InitiateTransactionOutput> InitiateTransaction(InitiateTransactionInput input)
//        {
//            var polyApiService = new PolipaymentService(ConfigSettings.PolipaymentApiBaseUrl);
//            var redirectUrl = polyApiService.InitiateTransaction(input.Amount, input.Currency);

//            var model = new InitiateTransactionOutput
//            {
//                RedirectUrl = redirectUrl
//            };

//            return ApiResult.Success(model);
//        }

//        [HttpGet]
//        public ApiResult<InitiateTransactionOutput> GetTransaction(GetTransactionInput input)
//        {
//            var polyApiService = new PolipaymentService(ConfigSettings.PolipaymentApiBaseUrl);
//            var redirectUrl = polyApiService.GetTransaction(input.TransactionToken);

//            var model = new InitiateTransactionOutput
//            {
//                RedirectUrl = redirectUrl
//            };

//            return ApiResult.Success(model);
//        }
//    }
//}