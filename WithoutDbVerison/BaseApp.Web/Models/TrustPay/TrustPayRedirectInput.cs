﻿
namespace AlphaPointPayment.Web.Models.TrustPay
{
    public class TrustPayRedirectInput
    {
        public string id { get; set; }

        public string resourcePath { get; set; }        
    }
}