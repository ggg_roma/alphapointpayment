﻿using System.ComponentModel.DataAnnotations;

namespace AlphaPointPayment.Web.Models.Razorpay
{
    public class RazorpayInitModel
    {
        public string PublicKey { get; set; }
        public string LogoUrl { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name ="Phone number")]
        [Required(ErrorMessage = "Mobile no. is required")]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone no.")]
        public string PhoneNumber { get; set; }

        public string Method { get; set; }

        [RegularExpression("[0-9]{0,}", ErrorMessage = "Please enter valid amount")]
        [Required(ErrorMessage = "Amount is required")]
        [Display(Name = "Amount(INR paise)")]
        public decimal Amount { get; set; }
    }
}