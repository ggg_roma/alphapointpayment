﻿using System.ComponentModel.DataAnnotations;

namespace AlphaPointPayment.Web.Models.Razorpay
{
    public class RazorpayResponse
    {
        public string razorpay_payment_id { get; set; }
    }
}