﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseApp.Web.Models.TemplateModels
{
    public class ResetPasswordModel
    {
        public string UserName { get; set; }
        public string ResetPasswordUrl { get; set; }
        public string RequestIp { get; set; }
    }
}