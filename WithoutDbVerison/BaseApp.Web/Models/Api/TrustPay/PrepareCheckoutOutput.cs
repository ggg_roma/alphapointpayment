﻿using BaseApp.Common;

namespace AlphaPointPayment.Web.Models.Api.TrustPay
{
    public class PrepareCheckoutOutput
    {
        public PrepareCheckoutOutput(string checkoutId, string description)
        {
            CheckoutId = checkoutId;
            TrustPayDescription = description;
        }

        public string CheckoutId { get; set; }

        public string TrustPayCode { get; set; }

        public string TrustPayDescription { get; set; }

        public string ScriptSrc => $"{ConfigSettings.TrustPayBaseUrl}v1/paymentWidgets.js?checkoutId={CheckoutId}";

    }
}