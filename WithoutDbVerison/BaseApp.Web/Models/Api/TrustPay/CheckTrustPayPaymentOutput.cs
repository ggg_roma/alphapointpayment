﻿
namespace AlphaPointPayment.Web.Models.Api.TrustPay
{
    public class CheckTrustPayPaymentOutput
    {
        public string Message { get; set; }
    }
}