﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using System.Web;
//using BaseApp.Common.Extensions;
//using BaseApp.Data.Infrastructure;
//using BaseApp.Web.Areas.HelpPage.Code;
//using BaseApp.Web.Code.Infrastructure;
//using MultipartDataMediaFormatter.Infrastructure;

//namespace BaseApp.Web.Models.Api
//{
//    public class GetUserArgs : ValidatableModelBase
//    {
//        [NotDefaultValueRequired]
//        public int UserId { get; set; }

//        protected override IEnumerable<ValidationResult> Validate(UnitOfWork unitOfWork, LoggedUserInfo currentUser, ValidationContext validationContext)
//        {
//            if (UserId != -1)
//            {
//                yield return new ValidationResult(String.Format("Test Message: User not found. Id - {0}", UserId), new []
//                                                                                                                   {
//                                                                                                                       this.GetPropertyName(m => m.UserId)
//                                                                                                                   });
//            }
//        }
//    }

//    public class GetUserErrorTestArgs
//    {
//        public int UserId { get; set; }
//    }

//    public class SaveTestFileArgs : IMultipartData
//    {
//        /// <summary>
//        /// BIG file
//        /// </summary>
//        public HttpFile File { get; set; }
//        public List<HttpFile> Files { get; set; }
//    }

//    /// <summary>
//    /// save test model
//    /// </summary>
//    public class SaveTestArgs
//    {
//        /// <summary>
//        /// first string
//        /// </summary>
//        public string FirstString { get; set; }

//        /// <summary>
//        /// second string
//        /// </summary>
//        public string SecondString { get; set; }
//    }

//    public class UserModel
//    {
//        public int UserId { get; set; }
//        public string SomeTestData { get; set; }
//    }
//}