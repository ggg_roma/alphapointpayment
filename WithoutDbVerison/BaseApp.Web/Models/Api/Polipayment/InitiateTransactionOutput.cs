﻿namespace AlphaPointPayment.Web.Models.Api.Polipayment
{
    public class InitiateTransactionOutput
    {
        public string RedirectUrl { get; set; }
    }
}