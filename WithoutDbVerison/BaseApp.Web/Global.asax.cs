﻿using System;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Globalization;
using System.Text;
using AlphaPointPayment.Common;
using BaseApp.Common;
using BaseApp.Common.Utils;
using BaseApp.Web.App_Start;
using BaseApp.Web.Code.Filters;
using BaseApp.Web.Code.Infrastructure;

namespace BaseApp.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private static bool IS_APP_STARTED;

        protected void Application_Start()
        {
            try
            {
                CultureInfo.DefaultThreadCurrentCulture = CultureInfo.CurrentCulture;
                CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.CurrentUICulture;

                //DbInitializer.Init(ConfigSettings.DbDataInitStrategyType);

                //need for logger
                //Application["AppVersion"] = DbInitializer.GetLatestMigration();


                AreaRegistration.RegisterAllAreas();

                ControllerBuilder.Current.DefaultNamespaces.Add("BaseApp.Web.Controllers");

                GlobalConfiguration.Configure(WebApiConfig.Register);
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);
                MapInit.Init();
                PathConverter.InitInstance(HttpRuntime.AppDomainAppPath);

                IS_APP_STARTED = true;
            }
            catch
            {
                IS_APP_STARTED = false;
                // Make sure we try to run Application_Start again next request
                HttpRuntime.UnloadAppDomain();
                throw;
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            //LogonManager.SetLoggedUserInfo(new HttpContextWrapper(Context));
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            if (exception is HttpException
                && (exception as HttpException).GetHttpCode() == (int)HttpStatusCode.NotFound)
            {
                LogHolder.Http404Log.ErrorException("An unhandled exception has occurred.", exception);
            }
            else
            {
                LogHolder.MainLog.ErrorException("An unhandled exception has occurred.", exception);
            }

            //LogHolder.MainLog.ErrorException(exception.FormLogMessage("An unhandled exception has occurred."), exception);

            if (!IS_APP_STARTED)
            {
                //error in Application_Start (Response object is not ready yet and we cannot call custom error controller)
                return;
            }

            Server.ClearError();

            //render error
            WebActionResultBase.SetRequestResult(new ServerErrorResult(exception), new HttpContextWrapper(Context));
        }

        protected void Application_EndRequest()
        {
            var context = new HttpContextWrapper(Context);

            var webResult = WebActionResultBase.GetRequestResult(context);
            if (webResult != null)
            {
                webResult.ExecuteResult(context);
            }
            else
            {
                if (Context.Response.StatusCode == 302 && context.Request.IsAjaxRequest())
                {
                    Context.Response.Clear();
                    Context.Response.Write(new string(' ', 512)); //fix opera issue
                    Context.Response.Headers.Add("AjaxRedirect", "1");
                    Context.Response.StatusCode = 200;
                }
            }
        }

        /*protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }*/


        protected void Session_Start(object sender, EventArgs e)
        {
            try
            {
                //var unitOfWork = DependencyResolver.Current.GetService<IUnitOfWorkFactoryPerRequest>().UnitOfWork;
                //unitOfWork.TestConnection();
            }
            catch (Exception ex)
            {
                Session.Abandon();
                WebActionResultBase.SetRequestResult(new ServerErrorResult(ex), new HttpContextWrapper(Context));
            }
        }
    }
}