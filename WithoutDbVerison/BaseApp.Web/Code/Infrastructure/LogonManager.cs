﻿//using System;
//using System.Linq;
//using System.Web;
//using System.Web.Caching;
//using System.Web.Mvc;
//using System.Web.Security;
//using BaseApp.Web.Models;

//namespace BaseApp.Web.Code.Infrastructure
//{
//    public static class LogonManager
//    {
//        public static void SetLoggedUserInfo(HttpContextBase httpContext)
//        {
//            if (httpContext.User != null && httpContext.User.Identity.IsAuthenticated)
//            {
//                string key = GetCacheKey(httpContext.User.Identity.Name);
//                LoggedUserInfo info = (LoggedUserInfo)HttpRuntime.Cache[key];
//                if (info == null)
//                {
//                    info = GetUserInfoFromDB(httpContext.User.Identity.Name);
//                    if (info != null)
//                    {
//                        HttpRuntime.Cache.Add(key, info, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, 20, 0), CacheItemPriority.Normal, null);
//                    }
//                }

//                if (info == null)
//                {
//                    SignOutUser(httpContext);
//                }
//                else
//                {
//                    httpContext.User = CreateUserPrincipal(info);
//                }
//            }
//        }

//        public static void RefreshCurrentLoggedUserInfo(HttpContextBase httpContext, string newLogin = null)
//        {
//            UserPrincipal userPrincipal = (UserPrincipal)httpContext.User;
//            if(userPrincipal != null)
//            {
//                string oldCacheKey = GetCacheKey(userPrincipal.Identity.Name);

//                HttpRuntime.Cache.Remove(oldCacheKey); 

//                if(!String.IsNullOrWhiteSpace(newLogin) && !String.Equals(newLogin, userPrincipal.Identity.Name, StringComparison.OrdinalIgnoreCase))
//                {
//                    //need reset auth ticket

//                    string newKey = GetCacheKey(newLogin);
//                    if(HttpRuntime.Cache[newKey] != null)
//                    {
//                        throw new Exception("Key already exists in cache - " + newKey);
//                    }
//                    LoggedUserInfo info = GetUserInfoFromDB(newLogin);
//                    if (info != null)
//                    {
//                        HttpRuntime.Cache.Add(newKey, info, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, 20, 0), CacheItemPriority.Normal, null);

//                        //remove old auth ticket
//                        FormsAuthentication.SignOut();

//                        //set new auth ticket
//                        FormsAuthentication.SetAuthCookie(newLogin, false);

//                        httpContext.User = CreateUserPrincipal(info);
//                    }
//                    else
//                    {
//                        throw new Exception("User not found - " + newLogin);
//                    }
//                }
//                else
//                {
//                    LoggedUserInfo info = GetUserInfoFromDB(userPrincipal.Identity.Name);
//                    if (info != null)
//                    {
//                        HttpRuntime.Cache.Add(oldCacheKey, info, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, 20, 0), CacheItemPriority.Normal, null);
//                        httpContext.User = CreateUserPrincipal(info);
//                    }
//                    else
//                    {
//                        throw new Exception("User not found - " + userPrincipal.Identity.Name);   
//                    }
//                }
//            }
//        }

//        public static void SignOut(HttpContextBase httpContext)
//        {
//            UserPrincipal userPrincipal = (UserPrincipal)httpContext.User;
//            if (userPrincipal != null)
//            {
//                string oldCacheKey = GetCacheKey(userPrincipal.Identity.Name);
//                HttpRuntime.Cache.Remove(oldCacheKey);
//                SignOutUser(httpContext);
//            }
//        }

//        private static void SignOutUser(HttpContextBase httpContext)
//        {
//            httpContext.User = null;
//            if (httpContext.Session != null)
//            {
//                httpContext.Session.Abandon();
//            }
//            System.Web.Security.FormsAuthentication.SignOut();
//        }

//        private static UserPrincipal CreateUserPrincipal(LoggedUserInfo user)
//        {
//            var identity = new UserIdentity("DB", true, user.Login);
//            return new UserPrincipal(identity, user);
//        }

//        private static LoggedUserInfo GetUserInfoFromDB(string username)
//        {
//            LoggedUserInfo info = null;
//            var unitOfWork = DependencyResolver.Current.GetService<IUnitOfWorkFactoryPerRequest>().UnitOfWork;

//            AccountProjection user = unitOfWork.Users.GetAccountByLoginOrNull(username);
//            if (user != null)
//            {
//                info = new LoggedUserInfo(user.Id, user.Login, user.FirstName, user.LastName, user.Roles.ToArray());
//            }
//            return info;
//        }

//        private static string GetCacheKey(string username)
//        {
//            if (String.IsNullOrWhiteSpace(username))
//                throw new ArgumentNullException("username");
//            return "LoggedUser." + username.ToLower();
//        }
//    }
//}