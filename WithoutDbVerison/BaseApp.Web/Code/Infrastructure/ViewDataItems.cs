﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BaseApp.Common;

namespace BaseApp.Web.Code.Infrastructure
{
    public class ViewDataItems
    {
        //private UnitOfWork UnitOfWork { get; set; }
        private Dictionary<string, object> Cache { get; set; }

        public ViewDataItems()
        {
            //if(unitOfWork == null)
            //    throw new ArgumentNullException("unitOfWork");
            //UnitOfWork = unitOfWork;
            Cache = new Dictionary<string, object>();
        }

        //public List<SelectListItem> Countries
        //{
        //    get
        //    {
        //        return Get("Countries", 
        //            () => UnitOfWork.Countries.GetCountries()
        //                .Select(m => new SelectListItem()
        //                                {
        //                                    Text = m.Name,
        //                                    Value = m.Id.ToString()
        //                                }).ToList());
        //    }
        //}

        //public List<SelectListItem> StatesOfUSA
        //{
        //    get
        //    {
        //        return Get("StatesOfUSA",
        //            () => UnitOfWork.Countries.GetStates(Constants.CountryUSA_Id)
        //                .Select(m => new SelectListItem()
        //                {
        //                    Text = m.Name,
        //                    Value = m.Id.ToString()
        //                }).ToList());
        //    }
        //}

        private T Get<T>(string key, Func<T> loadAction)
        {
            T res;
            Object obj;
            if(Cache.TryGetValue(key, out obj))
            {
                res = (T)obj;
            }
            else
            {
                res = loadAction();
                Cache.Add(key, res);
            }
            return res;
        }
    }
}