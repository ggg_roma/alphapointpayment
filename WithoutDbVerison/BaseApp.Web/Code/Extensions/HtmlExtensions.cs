﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using BaseApp.Common.Extensions;

namespace BaseApp.Web.Code.Extensions
{
    public static class HtmlExtensions
    {
       public static MvcHtmlString EnumDropDownListForEx<TModel, TEnum>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TEnum>> expression,
            object htmlAttributes = null,
            string defaultText = "Choose..."
            ) 
        {
           var listItems = GetDdlItems(typeof(TEnum), defaultText);

           var htmlString = htmlHelper.DropDownListFor(expression, listItems, htmlAttributes).ToHtmlString();

           return MvcHtmlString.Create(htmlString);
        }

        public static MvcHtmlString EnumDropDownListEx<TEnum>(
            this HtmlHelper htmlHelper,
            string name,
            object htmlAttributes = null,
            string defaultText = "Choose..."
            ) 
        {
            var listItems = GetDdlItems(typeof(TEnum), defaultText);

            var htmlString = htmlHelper.DropDownList(name, listItems, htmlAttributes).ToHtmlString();

            return MvcHtmlString.Create(htmlString);
        }

        private static List<SelectListItem> GetDdlItems(Type enumType, string defaultText)
        {
            enumType = Nullable.GetUnderlyingType(enumType) ?? enumType;

            var values = Enum.GetValues(enumType);
            var listItems = new List<SelectListItem>();
            for (int i = 0; i < values.Length; i++)
            {
                object value = values.GetValue(i);
                var item = new SelectListItem();
                item.Text = ((Enum)value).ToDescription();
                item.Value = ((Enum)value).ToString(); 
                listItems.Add(item);
            }

            if (defaultText != null)
            {
                listItems.Insert(0, new SelectListItem() { Text = defaultText, Value = "" });
            }
            return listItems;
        }   
    }
}