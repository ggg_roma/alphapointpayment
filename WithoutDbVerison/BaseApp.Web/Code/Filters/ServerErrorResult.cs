﻿using BaseApp.Common;
using BaseApp.Web.Controllers;
using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AlphaPointPayment.Common;

namespace BaseApp.Web.Code.Filters
{
    public class ServerErrorResult : WebActionResultBase
    {
        private readonly Exception Exception;

        public ServerErrorResult(Exception exception)
        {
            Exception = exception;
        }

        public override void ExecuteResult(HttpContextBase context)
        {
            if (context == null) throw new ArgumentNullException("context");

            context.Response.Clear();

            var routeData = new RouteData();
            routeData.Values["controller"] = "Errors";
            routeData.Values["action"] = "General";
            routeData.Values["exception"] = Exception;

            IController errorsController = new ErrorsController(context);
            var rc = new RequestContext(context, routeData);

            try
            {
                errorsController.Execute(rc);
            }
            catch (Exception ex)
            {
                LogHolder.MainLog.ErrorException("An unhandled exception has occurred.", ex);

                context.Response.HeaderEncoding = Encoding.UTF8;
                context.Response.ContentType = "text/html";
                context.Response.Write(String.Format("<html><body><h1>Error rendering error page.</h1><pre>{0}</pre></body></html>", ex));
                context.Response.End();
            }
        }
    }
}