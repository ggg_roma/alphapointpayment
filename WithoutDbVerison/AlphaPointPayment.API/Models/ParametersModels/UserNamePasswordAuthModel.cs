﻿namespace AlphaPointPayment.API.Models.ParametersModels
{
    public class UserNamePasswordAuthModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
