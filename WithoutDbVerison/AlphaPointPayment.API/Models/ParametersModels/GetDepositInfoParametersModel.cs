﻿namespace AlphaPointPayment.API.Models.ParametersModels
{
    public class GetDepositInfoParametersModel
    {
        public int OMSIs { get; set; }
        public int AccountId { get; set; }
        public int ProductId { get; set; }
    }
}
