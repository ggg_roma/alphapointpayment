﻿namespace AlphaPointPayment.API.Models.ResponseModels
{
    public class AlphaPointDepositInfo
    {
        public int AssetManagerId { get; set; }
        public int AccountId { get; set; }
        public int AssetId { get; set; }
        public int ProviderId { get; set; }
        public string DepositInfo { get; set; }
    }
}
