﻿namespace AlphaPointPayment.API.Models.ResponseModels
{
    public class AlphaPointProduct
    {
        public int OMSId { get; set; }
        public int ProductId { get; set; }
        public string Product { get; set; }
        public string ProductFullName { get; set; }
        public string ProductType { get; set; }
        public int DecimalPlaces { get; set; }
        public string TickSize { get; set; }
        public bool NoFees { get; set; }
    }
}
