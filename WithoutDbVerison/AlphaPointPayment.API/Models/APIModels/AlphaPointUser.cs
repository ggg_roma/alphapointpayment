﻿using System;

namespace AlphaPointPayment.API.Models.APIModels
{
    public class AlphaPointUser
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int AccountId { get; set; }
        public DateTime DateTimeCreated { get; set; }
        public int OMSId { get; set; }
    }
}
