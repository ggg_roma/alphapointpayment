﻿using AlphaPointPayment.API.Models;
using AlphaPointPayment.API.Models.ParametersModels;
using AlphaPointPayment.API.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AlphaPointPayment.API
{
    public class AlphaPointPublicAPI : IDisposable
    {
        private string AlphaPointSocket { get; set; }
        private string UserName { get; set; }
        private string Password { get; set; }

        #region ctors
        /// <summary>
        /// Initialize 
        /// </summary>
        public AlphaPointPublicAPI()
        {
            AlphaPointSocket = "ws://api.qa1.alphapoint.com:8086/WSGateway/";
        }

        /// <summary>
        /// Initialize Socket address by using string APISocketAddress
        /// </summary>
        /// <param name="APISocketAddress">Address of AlphaPoint WebSocket</param>
        public AlphaPointPublicAPI(string APISocketAddress)
        {
            AlphaPointSocket = APISocketAddress;
        }

        /// <summary>
        /// Initialize Socket
        /// </summary>
        /// <param name="APISocketAddress">Address of AlphaPoint WebSocket<</param>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        public AlphaPointPublicAPI(string APISocketAddress, string UserName, string Password)
        {
            AlphaPointSocket = APISocketAddress;
            this.UserName = UserName;
            this.Password = Password;
        }
        #endregion

        #region private methods
        /// <summary>
        /// Sends request to WebSocket API
        /// </summary>
        /// <param name="frame">All WebSocket Calls and Replies are embedded into a JSON-Formatted Frame object containing the relevant information about the call or reply, as well as the payload.</param>
        /// <returns>Returns Frame with API response data</returns>
        private string SendRequest(Frame frame)
        {
            try
            {
                using (var ws = new WebSocketSharp.WebSocket(AlphaPointSocket))
                {
                    TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();
                    ws.OnMessage += (sender, e) =>
                    {
                        tcs.SetResult(e.Data);
                    };
                    ws.Connect();
                    ws.Send(new JavaScriptSerializer().Serialize(frame));
                    return tcs.Task.Result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Forming request to API and handling response from API
        /// </summary>
        /// <typeparam name="T">Type of entity to handle response object</typeparam>
        /// <param name="requestFrame">Frame object containing the relevant information about the call or reply</param>
        /// <param name="parametersObject">Parameters object containing the data being sent with the message</param>
        /// <returns>Returns list of T entities</returns>
        private List<T> GetResponseList<T>(Frame requestFrame, object parametersObject)
        {
            List<T> resultList = new List<T>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            requestFrame.o = serializer.Serialize(parametersObject);

            string response = SendRequest(requestFrame);
            Frame result = serializer.Deserialize<Frame>(response);
            resultList = serializer.Deserialize<List<T>>(result.o);
            return resultList;
        }

        /// <summary>
        /// Forming request to API and handling response from API
        /// </summary>
        /// <typeparam name="T">Type of entity to handle response object</typeparam>
        /// <param name="requestFrame">Frame object containing the relevant information about the call or reply</param>
        /// <param name="parametersObject">Parameters object containing the data being sent with the message</param>
        /// <returns>Returns T entity</returns>
        private T GetResponseSingle<T>(Frame requestFrame, object parametersObject)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            requestFrame.o = serializer.Serialize(parametersObject);

            string response = SendRequest(requestFrame);
            Frame result = serializer.Deserialize<Frame>(response);
            T resultEntity = serializer.Deserialize<T>(result.o);
            return resultEntity;
        }

        /// <summary>
        /// Serialize object using JavaScriptSerializer
        /// </summary>
        /// <param name="o">Object to serialize</param>
        /// <returns></returns>
        private string SerializeObject(object o)
        {
            return new JavaScriptSerializer().Serialize(o);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="remoteFunction">Function name that will be called API</param>
        /// <returns></returns>
        private Frame GetRequestFrame(string remoteFunction)
        {
            return new Frame() { m = 0, i = 0, n = remoteFunction };
        }

        ////Signature is a HMAC-SHA256 encoded message containing: nonce, API Key and Client ID. The HMAC-SHA256 code must be generated using your API Secret Key.
        //This code must be converted to it's hexadecimal representation (64 uppercase characters).
        private string GenerateSignature(string ApiKey, string ApiSecretKey, int ClientId, int nonce)
        {
            var secret = ApiSecretKey ?? "";
            var message = nonce + ApiKey + ClientId;
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return BitConverter.ToString(hashmessage).Replace("-", ""); //Convert.ToBase64String(hashmessage);
            }
        }
        #endregion

        #region public methods
        /// <summary>
        /// Authentificate user connection to WebSocket
        /// </summary>
        /// <param name="userName">User login</param>
        /// <param name="password">User password</param>
        /// <returns></returns>
        public AuthenticateUserResponse AuthenticateUser(string userName, string password)
        {
            try
            {
                Frame f = GetRequestFrame("AuthenticateUser");
                UserNamePasswordAuthModel model = new UserNamePasswordAuthModel() { UserName = userName, Password = password };
                var result = GetResponseSingle<AuthenticateUserResponse>(f, model);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Authentificate user connection to WebSocket
        /// </summary>
        /// <param name="ApiKey">Public API Key</param>
        /// <param name="ApiSecretKey">API Secret Key</param>
        /// <param name="ClientId">Id of current client</param>
        /// <returns></returns>
        public AuthenticateUserResponse AuthenticateUser(string ApiKey, string ApiSecretKey, int ClientId)
        {
            try
            {
                Frame f = GetRequestFrame("AuthenticateUser");

                //Nonce is a regular integer number. It must increase with every request you make. A common practice is to use unix time for this parameter.
                var unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                var nonce = unixTimestamp;

                string signature = GenerateSignature(ApiKey, ApiSecretKey, ClientId, nonce);

                KeysAuthModel model = new KeysAuthModel() { APIKey = ApiKey, Nonce = nonce, Signature = signature};
                var result = GetResponseSingle<AuthenticateUserResponse>(f, model);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// You must call this in order to use any of the authenticated calls.
        /// </summary>
        /// <param name="userName">User login from AlphaPoint System</param>
        /// <param name="password">User password from AlphaPoint System</param>
        /// <returns></returns>
        public WebAuthenticateUserResponse WebAuthenticateUser(string userName, string password)
        {
            try
            {
                Frame f = GetRequestFrame("WebAuthenticateUser");
                UserNamePasswordAuthModel model = new UserNamePasswordAuthModel() { UserName = userName, Password = password };
                var result = GetResponseSingle<WebAuthenticateUserResponse>(f, model);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// End the current session
        /// </summary>
        /// <returns></returns>
        public bool LogOut()
        {
            bool result = false;
            try
            {
                Frame f = GetRequestFrame("LogOut");
                result = GetResponseSingle<StandardAPIResponse>(f, "{}").result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Get the available OMS's on the system you're connected to
        /// </summary>
        /// <param name="OperatorId">Identifier of current operator</param>
        /// <returns></returns>
        public List<AvailableOMS> GetOMSs(int OperatorId)
        {
            List<AvailableOMS> result = new List<AvailableOMS>();
            try
            {
                Frame f = GetRequestFrame("GetOMSs");
                GetOMSsParameterModel model = new GetOMSsParameterModel() { OperatorId = OperatorId };
                result = GetResponseList<AvailableOMS>(f, model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Requests a list of available Products from the API.
        /// </summary>
        /// <returns></returns>
        public List<AlphaPointProduct> GetProducts()
        {
            List<AlphaPointProduct> resultList = new List<AlphaPointProduct>();
            try
            {
                Frame f = GetRequestFrame("GetProducts");
                GetProductsParametersModel parameters = new GetProductsParametersModel() { OMSId = 1 };
                resultList = GetResponseList<AlphaPointProduct>(f, parameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultList;
        }

        /// <summary>
        /// Requests a list of available trading instruments (pairs) from the API.
        /// </summary>
        /// <returns></returns>
        public List<AlphaPointInstrument> GetInstruments()
        {
            List<AlphaPointInstrument> resultList = new List<AlphaPointInstrument>();
            try
            {
                Frame f = GetRequestFrame("GetInstruments");
                GetInstrumentsParametersModel parameters = new GetInstrumentsParametersModel() { OMSId = 1 };
                resultList = GetResponseList<AlphaPointInstrument>(f, parameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultList;
        }

        /// <summary>
        /// Get required deposit info to perform a deposit
        /// </summary>
        /// <param name="accountId">User account ID</param>
        /// <param name="productId">ID of deposite product</param>
        /// <returns></returns>
        public AlphaPointDepositInfo GetDepositInfo(int accountId, int productId)
        {
            AlphaPointDepositInfo result = new AlphaPointDepositInfo();
            try
            {
                //get deposit info
                Frame f = GetRequestFrame("GetDepositInfo");
                GetDepositInfoParametersModel model = new GetDepositInfoParametersModel() { OMSIs = 1, AccountId = accountId, ProductId = productId };
                result = GetResponseSingle<AlphaPointDepositInfo>(f, model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Create a deposit ticket to be tracked by AlphaPoint ticketing system
        /// </summary>
        /// <param name="accountId">User account ID</param>
        /// <param name="productId">ID of deposit product</param>
        /// <param name="amount">Ticket amount</param>
        /// <returns></returns>
        public AlphaPointCreateDepositTicketResponseModel CreateDepositTicket(int accountId, int productId, decimal amount)
        {
            AlphaPointCreateDepositTicketResponseModel response = new AlphaPointCreateDepositTicketResponseModel();
            response.success = false;
            response.requestcode = "Bad Request";

            try
            {
                //get deposit info
                var info = GetDepositInfo(accountId, productId);

                Frame f = GetRequestFrame("CreateDepositTicket");
                CreateDepositTicketParametersModel dtModel = new CreateDepositTicketParametersModel() { AccountId = accountId, OMSId = 1, Amount = amount, AssetId = info.AssetId, DepositInfo = SerializeObject(info) };
                response = GetResponseSingle<AlphaPointCreateDepositTicketResponseModel>(f, dtModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }


        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    LogOut();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }

}
