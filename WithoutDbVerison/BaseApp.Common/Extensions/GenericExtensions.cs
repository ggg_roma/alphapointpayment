﻿using System;
using System.Linq.Expressions;
using BaseApp.Common.Utils;

namespace BaseApp.Common.Extensions
{
    public static class GenericExtensions
    {
        public static string GetPropertyName<TObject>(this TObject type,
                                                       Expression<Func<TObject, object>> propertyRefExpr)
        {
            return PropertyUtil.GetName(propertyRefExpr);
        }
    }
}
