﻿using System.ComponentModel;

namespace BaseApp.Common
{
    public class Enums
    {
        public enum NotificationEmailTypes
        {
            ResetPassword = 1
        }

        public enum MenuItemTypes
        {
            
        }

        public enum DbDataInitStrategyTypes
        {
            MigrateValidate,
            MigrateToLatest,
            None
        }

        public enum SchedulerOnDateShiftTypes
        {
            //TODO: implement
        }

        public enum Currencies
        {
            [Description("US Dollar")]
            USD = 1,
            [Description("Canadian Dollar")]
            CAD,
            [Description("Euro")]
            EUR
        }

        public enum PaymentStatuses
        {
            New = 1,
            Submitted,
            Error,
            WrongComparedData
        }
    }
}
