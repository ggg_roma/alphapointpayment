﻿using System.ServiceModel;
using System.ServiceProcess;
using AlphapointPayment.TrustPay;
using AlphapointPayment.Razorpay;

namespace AlphapointPaymentWinService
{
    public partial class PaymentWinService : ServiceBase
    {
        ServiceHost _trustPayServiceHost = null;
        ServiceHost _razorpayServiceHost = null;

        public PaymentWinService()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            _trustPayServiceHost = new ServiceHost(typeof(TrustPayPaymentService));
            _trustPayServiceHost.Open();

            _razorpayServiceHost = new ServiceHost(typeof(RazorpayPaymentService));
            _razorpayServiceHost.Open();
        }

        protected override void OnStop()
        {
            if (_trustPayServiceHost != null)
            {
                _trustPayServiceHost.Close();
                _trustPayServiceHost = null;
            }

            if (_razorpayServiceHost != null)
            {
                _razorpayServiceHost.Close();
                _razorpayServiceHost = null;
            }
        }
    }
}
