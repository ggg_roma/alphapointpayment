﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace AlphapointPaymentWinService
{
    [RunInstaller(true)]
    public partial class PaymentWinServiceInstaller : System.Configuration.Install.Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public PaymentWinServiceInstaller()
        {
            //InitializeComponent();
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new ServiceInstaller();
            service.ServiceName = "AlphapointPaymentWinService";
            service.DisplayName = "AlphapointPaymentWinService";
            service.Description = "WCF REST API Hosting on Windows Service";
            service.DelayedAutoStart = true;
            Installers.Add(process);
            Installers.Add(service);
        }
    }
}
