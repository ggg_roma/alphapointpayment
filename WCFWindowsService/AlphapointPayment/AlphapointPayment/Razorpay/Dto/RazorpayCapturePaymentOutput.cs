﻿using System.Runtime.Serialization;

namespace AlphapointPayment.Razorpay.Dto
{
    [DataContract]
    public class RazorpayCapturePaymentOutput
    {
        [DataMember]
        public bool IsSuccess { get; set; }

        [DataMember]
        public string PaymentResultMessageText { get; set; }
    }
}
