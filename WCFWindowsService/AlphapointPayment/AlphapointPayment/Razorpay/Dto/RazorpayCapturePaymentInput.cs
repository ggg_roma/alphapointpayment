﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AlphapointPayment.Razorpay.Dto
{
    [DataContract]
    public class RazorpayCapturePaymentInput
    {
        [Required]
        [DataMember]
        public string RazorpayPaymentId { get; set; }

        [Required]
        [DataMember]
        public int AccountId { get; set; }

        [Required]
        [DataMember]
        public int ProductId { get; set; }
    }
}
