﻿using AlphapointPayment.Razorpay.Dto;
using AlphaPointPayment.Common;
using AlphaPointPayment.Web.Code.Services.Razorpay;
using System;
using System.ServiceModel;
using AlphapointPayment.Infrastructure;

namespace AlphapointPayment.Razorpay
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    public class RazorpayPaymentService : IRazorpayPaymentService
    {
        public ApiResult<RazorpayCapturePaymentOutput> CaptureRazorpayPayment(RazorpayCapturePaymentInput input)
        {
            RazorpayCapturePaymentOutput result = new RazorpayCapturePaymentOutput();
            try
            {
                RazorpayService service = new RazorpayService();
                var serviceResponse = service.CapturePaymentAndCreateDeposit(input.RazorpayPaymentId, input.AccountId, input.ProductId);
                result.IsSuccess = serviceResponse.IsSuccess;
                result.PaymentResultMessageText = serviceResponse.Message;

                LogHolder.PaymentLog.Info($"CaptureRazorpayPayment - RazorpayPaymentId: {input.RazorpayPaymentId}; Created deposit ticket number: {result.PaymentResultMessageText}");

                return ApiResult.Success(result);
            }
            catch (Exception ex)
            {
                LogHolder.PaymentLog.Error($"CaptureRazorpayPayment - RazorpayPaymentId: {input.RazorpayPaymentId}; Operation detail: {ex.ToString()}");
                return ApiResult.Error<RazorpayCapturePaymentOutput>(ApiResult.ApiResultCodes.Exception, ex.Message);
            }
        }
    }
}
