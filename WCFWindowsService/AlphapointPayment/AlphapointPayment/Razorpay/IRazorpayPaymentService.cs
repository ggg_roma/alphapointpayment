﻿using AlphapointPayment.Razorpay.Dto;
using System.ServiceModel;
using System.ServiceModel.Web;
using AlphapointPayment.Infrastructure;

namespace AlphapointPayment.Razorpay
{
    [ServiceContract]
    public interface IRazorpayPaymentService
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        ApiResult<RazorpayCapturePaymentOutput> CaptureRazorpayPayment(RazorpayCapturePaymentInput input);
    }
}
