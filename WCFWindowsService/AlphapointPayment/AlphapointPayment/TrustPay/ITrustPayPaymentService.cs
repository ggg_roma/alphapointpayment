﻿using AlphapointPayment.TrustPay.Dto;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Threading.Tasks;
using AlphapointPayment.Infrastructure;

namespace AlphapointPayment.TrustPay
{
    [ServiceContract]
    public interface ITrustPayPaymentService
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        ApiResult<PrepareCheckoutOutput> PrepareCheckout(PrepareCheckoutInput input);

        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        Task<ApiResult<VerifyTrustPayPaymentOutput>> VerifyTrustPayPayment(VerifyTrustPayPaymentInput input);
    }
}
