﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.ServiceModel;
using AlphapointPayment.TrustPay.Dto;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using AlphapointPayment.Infrastructure;
using AlphaPointPayment.API;
using AlphaPointPayment.Common;
using AlphaPointPayment.Services.TrustPay;
using AlphaPointPayment.Services.TrustPay.Dto;

namespace AlphapointPayment.TrustPay
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    public class TrustPayPaymentService : ITrustPayPaymentService
    {
        public ApiResult<PrepareCheckoutOutput> PrepareCheckout(PrepareCheckoutInput input)
        {
            var currency = (Enums.Currencies)Enum.Parse(typeof(Enums.Currencies), input.Currency);
            var paymentType = (Enums.TrustPayPaymentTypes)Enum.Parse(typeof(Enums.TrustPayPaymentTypes), input.PaymentType);

            var trustPayService = new TrustPayService(ConfigSettings.TrustPayBaseUrl);
            var alphapointApi = new AlphaPointPublicAPI(ConfigSettings.AlphaPointAPI_Url, "Starpoint2", "Alpha123");
            var checkoutData = trustPayService.PrepareCheckout(input.Amount, currency, paymentType);
            var depositData = alphapointApi.CreateDepositTicket(input.AccountId, input.ProductId, input.Amount);
            var model = new PrepareCheckoutOutput
            {
                CheckoutId = checkoutData.ndc,
                ResultCode = checkoutData.result.code,
                ResultDescription = checkoutData.result.description,
                DepositTicketRequestCode = depositData.requestcode,
                IsCreateDepositTicketSuccess = depositData.success
            };
            LogHolder.PaymentLog.Info($"PrepareCheckout - IsCreateDepositTicketSuccess: {model.IsCreateDepositTicketSuccess}; CheckoutId: {model.CheckoutId}; " +
                $"ResultCode: {model.ResultCode}; ResultDescription: {model.ResultDescription }; DepositTicketRequestCode: {model.DepositTicketRequestCode}");

            return ApiResult.Success(model);
        }

        public async Task<ApiResult<VerifyTrustPayPaymentOutput>> VerifyTrustPayPayment(VerifyTrustPayPaymentInput input)
        {
            var trustPayService = new TrustPayService(ConfigSettings.TrustPayBaseUrl);

            Dictionary<string, dynamic> responseData;
            string data0 = "authentication.userId=8a8294174e735d0c014e78e083bc197b" +
                          "&authentication.password=K4ceFFSmYA" +
                          "&authentication.entityId=8a8294174e735d0c014e78e0839f1977";
            string url = "https://test.oppwa.com/v1/checkouts/CBE269E74484009F53A70C26DD18E439.sbg-vm-tx01/payment?" + data0;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "GET";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                var s = new JavaScriptSerializer();
                responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                reader.Close();
                dataStream.Close();
            }

            var data = await trustPayService.GetPaymentDataAsync(input.ResourcePath);
            
            if (!data.result.code.StartsWith("000.000"))
            {
                var errorMessage =
                    $"Error while getting Trust Pay data: {data.result.code} - {data.result.description}";
                LogHolder.PaymentLog.Error(errorMessage);
                return ApiResult.Error<VerifyTrustPayPaymentOutput>(ApiResult.ApiResultCodes.Exception, errorMessage);
            }
            if (input.CheckoutId != data.ndc)
            {
                var errorMessage = "Wrong CheckoutId, please try again!";
                LogHolder.PaymentLog.Error(errorMessage);
                return ApiResult.Error<VerifyTrustPayPaymentOutput>(ApiResult.ApiResultCodes.Exception, errorMessage);
            }
            var alphapointApi = new AlphaPointPublicAPI(ConfigSettings.AlphaPointAPI_Url, "Starpoint2", "Alpha123");
            var updateTicketData = alphapointApi.UpdateDepositTicket(input.AccountId, Enums.ApDepositStatus.Accepted,
                requestCode: input.DeposiTicketRequestCode);

            if (updateTicketData.result == false)
            {
                var errorMessage = $"Error while deposit ticket updating: {updateTicketData.errorcode} - {updateTicketData.detail}";
                LogHolder.PaymentLog.Error(errorMessage);
                return ApiResult.Error<VerifyTrustPayPaymentOutput>(ApiResult.ApiResultCodes.Exception, errorMessage);
            }
            LogHolder.PaymentLog.Info($"VerifyTrustPayPayment - CheckoutId: {input.CheckoutId}; UpdateDepositTicket detail: {updateTicketData.detail}");

            var model = new VerifyTrustPayPaymentOutput
            {
                PaymentResultMessageText = "Success"
            };
            return ApiResult.Success(model);
        }
    }
}
