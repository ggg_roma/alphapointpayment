﻿using AlphaPointPayment.Common;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AlphapointPayment.TrustPay.Dto
{
    [DataContract]
    public class PrepareCheckoutInput
    {
        [Required]
        [DataMember]
        public int AccountId { get; set; }

        [Required]
        [DataMember]
        public int ProductId { get; set; }

        [Required]
        [DataMember]
        public decimal Amount { get; set; }

        [Required]
        [DataMember]
        public string Currency { get; set; }

        [Required]
        [DataMember]
        public string PaymentType { get; set; }
    }
}