﻿using System.Runtime.Serialization;

namespace AlphapointPayment.TrustPay.Dto
{
    [DataContract]
    public class VerifyTrustPayPaymentOutput
    {
        [DataMember]
        public string PaymentResultMessageText { get; set; }
    }
}
