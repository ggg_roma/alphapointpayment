﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AlphapointPayment.TrustPay.Dto
{
    [DataContract]
    public class VerifyTrustPayPaymentInput
    {
        [Required]
        [DataMember]
        public string ResourcePath { get; set; }

        [Required]
        [DataMember]
        public string CheckoutId { get; set; }

        [Required]
        [DataMember]
        public int AccountId { get; set; }

        [Required]
        [DataMember]
        public string DeposiTicketRequestCode { get; set; }
    }
}
