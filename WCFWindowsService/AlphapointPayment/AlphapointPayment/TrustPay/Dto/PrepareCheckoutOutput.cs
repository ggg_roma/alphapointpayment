﻿using AlphaPointPayment.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlphapointPayment.TrustPay.Dto
{
    [DataContract]
    public class PrepareCheckoutOutput
    {
        [DataMember]
        public string CheckoutId { get; set; }

        [DataMember]
        public string ResultCode { get; set; }

        [DataMember]
        public string ResultDescription { get; set; }

        [DataMember]
        public string DepositTicketRequestCode { get; set; }

        [DataMember]
        public bool IsCreateDepositTicketSuccess { get; set; }

        [DataMember]
        public string ScriptSrc
        {
            get { return $"{ConfigSettings.TrustPayBaseUrl}v1/paymentWidgets.js?checkoutId={CheckoutId}"; }
            set { }
        }
    }
}
