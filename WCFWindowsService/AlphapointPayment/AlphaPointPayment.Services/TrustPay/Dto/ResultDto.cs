﻿namespace AlphaPointPayment.Services.TrustPay.Dto
{
    public class ResultDto
    {
        public string code { get; set; }
        public string description { get; set; }
    }
}