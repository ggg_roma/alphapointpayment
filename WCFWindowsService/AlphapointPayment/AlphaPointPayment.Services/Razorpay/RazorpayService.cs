﻿using AlphaPointPayment.API;
using AlphaPointPayment.Common;
using AlphaPointPayment.Services.Razorpay.Dto;
using Razorpay.Api;
using System;
using System.Collections.Generic;

namespace AlphaPointPayment.Web.Code.Services.Razorpay
{
    /// <summary>
    /// Razorpay payment service
    /// </summary>
    public class RazorpayService
    {
        #region properties
        private string key_id { get; set; }
        private string key_secret { get; set; }
        private string alphaPointAPIAddress { get; set; }
        #endregion

        #region ctor
        public RazorpayService()
        {
            key_id = ConfigSettings.RazorPayKey_id;
            key_secret =  ConfigSettings.RazorPayKey_secret;
            alphaPointAPIAddress = ConfigSettings.AlphaPointAPI_Url;
        }
        #endregion

        #region Public methods
        public RazorpayCapturePaymentResponse CapturePaymentAndCreateDeposit(string razorpayPaymentId, int alphaPointAccountId, int productId)
        {
            RazorpayCapturePaymentResponse result = new RazorpayCapturePaymentResponse();
            try
            {
                RazorpayClient client = new RazorpayClient(key_id, key_secret);
                Payment payment = client.Payment.Fetch(razorpayPaymentId);

                decimal amount;
                if (!Decimal.TryParse(payment.Attributes["amount"].ToString(), out amount)) throw new ArgumentException("Razorpay payment amount must be decimal value.");

                Dictionary<string, object> options = new Dictionary<string, object>();
                options.Add("amount", payment.Attributes["amount"]);

                var capturePaymentResult = payment.Capture(options);

                if (!(capturePaymentResult is Payment) || capturePaymentResult["error_code"] != null)
                {
                    result.IsSuccess = false;
                    result.Message = "Razorpay error_code: " + capturePaymentResult["error_code"] + "; Razorpay error_description: " + capturePaymentResult["error_description"];
                }

                var alphapointAnswer =  new AlphaPointPublicAPI(alphaPointAPIAddress).CreateDepositTicket(alphaPointAccountId, productId, amount);
                result.IsSuccess = alphapointAnswer.success;
                result.Message = alphapointAnswer.requestcode;
                return result;
            }
            catch { throw; }
        }
        #endregion
    }
}