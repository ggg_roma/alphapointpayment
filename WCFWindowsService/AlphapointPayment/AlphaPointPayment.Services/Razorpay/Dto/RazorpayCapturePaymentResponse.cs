﻿namespace AlphaPointPayment.Services.Razorpay.Dto
{
    public class RazorpayCapturePaymentResponse
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
