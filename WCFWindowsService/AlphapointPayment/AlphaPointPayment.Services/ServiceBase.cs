﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AlphaPointPayment.Services
{
    public class ServiceBase
    {
        public string _baseUrl { get; set; }

        public ServiceBase(string baseUrl)
        {
            _baseUrl = baseUrl;
        }
        
        protected T PerformGetRequest<T>(string command)
        {
            return PerformGetRequestAsync<T>(command).Result;
        }

        protected async Task<T> PerformGetRequestAsync<T>(string command)
        {
            HttpMethod method = HttpMethod.Get;
            string content = "";
            string url = "";
            try
            {
                using (var hc = GetClient())
                {
                    url = hc.BaseAddress + command;
                    var task = await
                        hc.SendAsync(new HttpRequestMessage(method, command)).ConfigureAwait(false);

                    content = await task.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var parsed = JsonConvert.DeserializeObject<T>(content, new JsonSerializerSettings());
                    task.EnsureSuccessStatusCode();
                    return parsed;
                }
            }
            catch (Exception ex)
            {
                string messageText = string.Format("Error. HttpMethod: {0}, Url:{1}, Content: {2}", method, url, content);
                throw new Exception(messageText, ex);
            }
        }

        protected void PerformSubmitRequest<T>(string command, T submitData, HttpMethod method, string contentType = "application/json", bool isPatchRequest = false)
        {
            PerformSubmitRequestAsync<T, string>(command, submitData, method, contentType, isPatchRequest).Wait();
        }

        protected TResult PerformSubmitRequest<T, TResult>(string command, T submitData, HttpMethod method, string contentType = "application/json", bool isPatchRequest = false)
        {
            return PerformSubmitRequestAsync<T, TResult>(command, submitData, method, contentType, isPatchRequest).Result;
        }

        protected async Task<TResult> PerformSubmitRequestAsync<T, TResult>(string command, T submitData, HttpMethod method, string contentType = "application/json",
            bool isPatchRequest = false)
        {
            string content = "";
            string url = "";
            try
            {
                using (var hc = GetClient())
                {
                    url = hc.BaseAddress + command;

                    string serialized = "";
                    if (submitData != null)
                    {
                        serialized = JsonConvert.SerializeObject(submitData);
                    }

                    HttpContent submitContent = new StringContent(serialized, Encoding.UTF8, contentType);

                    HttpResponseMessage task;
                    if (isPatchRequest)
                    {
                        var nMethod = new HttpMethod("PATCH");
                        var request = new HttpRequestMessage(nMethod, command)
                        {
                            Content = submitContent
                        };
                        task = await hc.SendAsync(request).ConfigureAwait(false);
                    }
                    else
                    if (method == HttpMethod.Put)
                    {
                        task = await hc.PutAsync(command, submitContent).ConfigureAwait(false);
                    }
                    else if (method == HttpMethod.Post)
                    {
                        task = await hc.PostAsync(command, submitContent).ConfigureAwait(false);
                    }
                    else if (method == HttpMethod.Delete)
                    {
                        task = await hc.DeleteAsync(command).ConfigureAwait(false);
                    }
                    else
                    {
                        throw new Exception(string.Format("PerformSubmitRequestAsync does not support {0} method", method));
                    }
                    content = await task.Content.ReadAsStringAsync().ConfigureAwait(false);

                    task.EnsureSuccessStatusCode();
                    if (typeof(TResult) == typeof(string))
                        return (TResult)((object)content);

                    return JsonConvert.DeserializeObject<TResult>(content, new JsonSerializerSettings()); ;
                }
            }
            catch (Exception ex)
            {
                string messageText = string.Format("Layer Error. HttpMethod: {0}, Url:{1}, Content: {2}", method, url, content);
                throw new Exception(messageText, ex);
            }
        }


        protected string FormCommand(string command, NameValueCollection parameters = null)
        {
            if (parameters == null || parameters.Count == 0)
                return command;

            return string.Format("{0}?{1}", command, ConstructQueryString(parameters));
        }

        protected string ConstructQueryString(NameValueCollection parameters)
        {
            return String.Join("&", (from string name in parameters select String.Concat(name, "=", HttpUtility.UrlEncode(parameters[name]))).ToArray());
        }

        private HttpClient GetClient()
        {
            HttpClientHandler handler = new HttpClientHandler();
            HttpClient client = new HttpClient(handler);
            client.BaseAddress = new Uri(_baseUrl);
            //string authorization = String.Concat("Bearer ", ConfigSettings.LayerTOKEN);
            //client.DefaultRequestHeaders.Add("Authorization", authorization);
            //client.DefaultRequestHeaders.Add("Accept", "application/vnd.layer+json; version=1.1");

            return client;
        }
    }
}