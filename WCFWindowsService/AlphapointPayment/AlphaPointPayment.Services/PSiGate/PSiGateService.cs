﻿using AlphaPointPayment.Common;
using AlphaPointPayment.Common.Utils;
using AlphaPointPayment.Web.Models.Api.PSiGate;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using static AlphaPointPayment.Common.Enums;

namespace AlphaPointPayment.Web.Code.Services.PSiGate
{
    //TODO TEST PSiGate
    /// <summary>
    /// PSI Gate payment service
    /// </summary>
    public class PSiGateService
    {
        #region Fields
        private bool useSandBox = true;
        private string storeID;
        private string passphrase;
        #endregion

        #region Methods

        /// <summary>
        /// Initializes the Authorize.NET payment processor
        /// </summary>
        private void InitSettings()
        {
            useSandBox = ConfigSettings.PSiGateUseSendbox;
            storeID =  ConfigSettings.PSiGateStoreID;
            passphrase = ConfigSettings.PSiGatePassphrase;
        }

        /// <summary>
        /// Gets PSI Gate URL
        /// </summary>
        /// <returns></returns>
        private string GetPSIGateUrl()
        {
            return useSandBox ? ConfigSettings.PSiGateAPIUrlTestEnvironment :
                ConfigSettings.PSiGateAPIUrlProductionEnvironment;
        }

        /// <summary>
        /// Process payment
        /// </summary>
        /// <param name="paymentInfo">Payment info required for an order processing</param>
        /// <param name="customer">Customer</param>
        /// <param name="orderGuid">Unique order identifier</param>
        /// <param name="processPaymentResult">Process payment result</param>
        public void ProcessPayment(PSiGatePaymentInfo paymentInfo, PSiGateCustomer customer, Guid orderGuid, ref PsiGateProcessPaymentResult processPaymentResult)
        {
            InitSettings();

            StringBuilder builder = new StringBuilder();
            builder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            builder.Append("<Order>");
            builder.Append("<StoreID>" + XmlHelper.XmlEncode(storeID) + "</StoreID>");
            builder.Append("<Passphrase>" + XmlHelper.XmlEncode(passphrase) + "</Passphrase>");
            builder.Append("<Bname>" + XmlHelper.XmlEncode(paymentInfo.BillingAddress.FirstName) + "</Bname>");
            builder.Append("<Bcompany>" + XmlHelper.XmlEncode(paymentInfo.BillingAddress.Company) + "</Bcompany>");
            builder.Append("<Baddress1>" + XmlHelper.XmlEncode(paymentInfo.BillingAddress.Address1) + "</Baddress1>");
            builder.Append("<Baddress2>" + XmlHelper.XmlEncode(paymentInfo.BillingAddress.Address2) + "</Baddress2>");
            builder.Append("<Bcity>" + XmlHelper.XmlEncode(paymentInfo.BillingAddress.City) + "</Bcity>");
            if (paymentInfo.BillingAddress.StateProvince != null)
                builder.Append("<Bprovince>" + XmlHelper.XmlEncode(paymentInfo.BillingAddress.StateProvince) + "</Bprovince>");
            builder.Append("<Bpostalcode>" + XmlHelper.XmlEncode(paymentInfo.BillingAddress.ZipPostalCode) + "</Bpostalcode>");
            if (paymentInfo.BillingAddress.Country != null)
                builder.Append("<Bcountry>" + XmlHelper.XmlEncode(paymentInfo.BillingAddress.Country) + "</Bcountry>");
            builder.Append("<Phone>" + XmlHelper.XmlEncode(paymentInfo.BillingAddress.PhoneNumber) + "</Phone>");
            builder.Append("<Fax>" + XmlHelper.XmlEncode(paymentInfo.BillingAddress.FaxNumber) + "</Fax>");
            builder.Append("<Email>" + XmlHelper.XmlEncode(paymentInfo.BillingAddress.Email) + "</Email>");
            builder.Append("<Comments> </Comments>");
            builder.Append("<CustomerIP>" + XmlHelper.XmlEncode(HttpContext.Current.Request.UserHostAddress) + "</CustomerIP>");
            builder.Append("<Subtotal>" + XmlHelper.XmlEncode(paymentInfo.OrderTotal.ToString("N2", new CultureInfo("en-US", false).NumberFormat)) + "</Subtotal>");
            builder.Append("<PaymentType>CC</PaymentType>");
            builder.Append("<CardAction>0</CardAction>");
            builder.Append("<CardNumber>" + XmlHelper.XmlEncode(paymentInfo.CreditCardNumber) + "</CardNumber>");
            string cardExpMonth = string.Empty;
            if (paymentInfo.CreditCardExpireMonth < 10)
            {
                cardExpMonth = "0" + paymentInfo.CreditCardExpireMonth.ToString();
            }
            else
            {
                cardExpMonth = paymentInfo.CreditCardExpireMonth.ToString();
            }
            builder.Append("<CardExpMonth>" + XmlHelper.XmlEncode(cardExpMonth) + "</CardExpMonth>");
            builder.Append("<CardExpYear>" + XmlHelper.XmlEncode(paymentInfo.CreditCardExpireYear.ToString().Substring(2, 2)) + "</CardExpYear>");
            builder.Append("<CardIDNumber>" + XmlHelper.XmlEncode(paymentInfo.CreditCardCvv2) + "</CardIDNumber>");
            builder.Append("</Order>");
            string orderInfo = builder.ToString();

            byte[] bytes = Encoding.ASCII.GetBytes(orderInfo);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(GetPSIGateUrl());
            request.Method = "POST";
            request.ContentLength = bytes.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/xml; charset=UTF-8";
            request.KeepAlive = false;

            using (Stream str0 = request.GetRequestStream())
                str0.Write(bytes, 0, bytes.Length);

            string xmlResponse = string.Empty;
            try
            {
                using (HttpWebResponse resp1 = (HttpWebResponse)request.GetResponse())
                using (Stream str1 = resp1.GetResponseStream())
                using (StreamReader reader = new StreamReader(str1))
                    xmlResponse = reader.ReadToEnd();
            }
            catch (WebException exc)
            {
                if (exc.Response != null)
                    using (HttpWebResponse resp2 = (HttpWebResponse)exc.Response)
                    using (StreamReader rdr2 = new StreamReader(resp2.GetResponseStream()))
                        xmlResponse = rdr2.ReadToEnd();
            }
            StringReader input = new StringReader(xmlResponse);
            XmlTextReader rdr3 = new XmlTextReader(input);

            Label_1:
            while (rdr3.Read())
            {
                if (!(rdr3.IsStartElement() & (rdr3.Name == "Result")))
                {
                    continue;
                }
                while (rdr3.Read())
                {
                    if ((rdr3.NodeType == XmlNodeType.Element) && (rdr3.Name == "OrderID"))
                    {
                        processPaymentResult.AuthorizationTransactionId = rdr3.ReadElementString("OrderID");
                    }
                    else
                    {
                        if ((rdr3.NodeType == XmlNodeType.Element) && (rdr3.Name == "Approved"))
                        {
                            string approvedString = rdr3.ReadElementString("Approved");
                            if (approvedString == "APPROVED")
                                processPaymentResult.PaymentStatus = PaymentStatusPCiGate.Paid;

                            continue;
                        }
                        if ((rdr3.NodeType == XmlNodeType.Element) && (rdr3.Name == "ReturnCode"))
                        {
                            processPaymentResult.AuthorizationTransactionCode = rdr3.ReadElementString("ReturnCode");
                            continue;
                        }
                        if ((rdr3.NodeType == XmlNodeType.Element) && (rdr3.Name == "ErrMsg"))
                        {
                            processPaymentResult.Error = rdr3.ReadElementString("ErrMsg");
                            continue;
                        }
                        if ((rdr3.Name == "Result") & (rdr3.NodeType == XmlNodeType.EndElement))
                        {
                            goto Label_1;
                        }
                    }
                }
            }

            if (processPaymentResult.PaymentStatus != PaymentStatusPCiGate.Paid && string.IsNullOrEmpty(processPaymentResult.Error))
                processPaymentResult.Error = "Unknown PSI Gate error";
        }
        #endregion
    }
}