﻿using static AlphaPointPayment.Common.Enums;

namespace AlphaPointPayment.Web.Models.Api.PSiGate
{
    public class PsiGateProcessPaymentResult
    {
        public string AuthorizationTransactionId { get; set; }
        public PaymentStatusPCiGate PaymentStatus { get; set; }
        public string AuthorizationTransactionCode { get; set; }
        public string Error { get; set; }
    }
}