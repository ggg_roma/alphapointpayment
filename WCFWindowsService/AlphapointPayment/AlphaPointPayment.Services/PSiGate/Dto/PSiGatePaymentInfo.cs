﻿namespace AlphaPointPayment.Web.Models.Api.PSiGate
{
    public class PSiGatePaymentInfo
    {
        public PSiGateBillingAddress BillingAddress { get; set; }
        public decimal OrderTotal { get; set; }
        public int CreditCardExpireMonth { get; set; }
        public string CreditCardNumber { get; set; }
        public int CreditCardExpireYear { get; set; }
        public string CreditCardCvv2 { get; set; }
    }
}