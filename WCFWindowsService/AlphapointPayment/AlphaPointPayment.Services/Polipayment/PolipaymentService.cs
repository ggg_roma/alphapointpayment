﻿using AlphaPointPayment.Common;
using AlphaPointPayment.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AlphaPointPayment.Web.Code.Services.Polipayment
{
    public class PolipaymentService : ServiceBase
    {
        public PolipaymentService(string baseUrl)
            : base(baseUrl)
        {          
        }

        public string InitiateTransaction(decimal amount, Enums.Currencies currency)
        {
            string returnUrl = "";

            var json = System.Text.Encoding.UTF8.GetBytes(@"{
              'Amount':'" + amount + @"',
              'CurrencyCode':'" + currency + @"',
              'MerchantReference':'" + ConfigSettings.PolipaymentApiMerchantReference + @"',
              'MerchantHomepageURL':'" + ConfigSettings.PolipaymentApiMerchantHomepageURL + @"',
              'SuccessURL':'" + ConfigSettings.PolipaymentApiSuccessURL + @"',
              'FailureURL':'" + ConfigSettings.PolipaymentApiFailureURL + @"',
              'CancellationURL':'" + ConfigSettings.PolipaymentApiCancellationURL + @"',
              'NotificationURL':'" + ConfigSettings.PolipaymentApiNotificationURL + @"'
            //}");
            var auth = 
                System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes($"{ ConfigSettings.PolipaymentApiMerchantCode }:{ ConfigSettings.PolipaymentApiAuthCode }"));

            var myRequest =
                System.Net.WebRequest.Create($"{_baseUrl}api/v2/Transaction/Initiate");
            myRequest.Method = "POST";
            myRequest.ContentType = "application/json";
            myRequest.Headers.Add("Authorization", "Basic " + auth);
            myRequest.ContentLength = json.Length;

            Stream dataStream = myRequest.GetRequestStream();
            dataStream.Write(json, 0, json.Length);
            dataStream.Close();

            var response = (System.Net.HttpWebResponse)myRequest.GetResponse();
            var data = response.GetResponseStream();
            var streamRead = new StreamReader(data);
            Char[] readBuff = new Char[response.ContentLength];
            int count = streamRead.Read(readBuff, 0, (int)response.ContentLength);
            while (count > 0)
            {
                var outputData = new String(readBuff, 0, count);
                Console.Write(outputData);
                count = streamRead.Read(readBuff, 0, (int)response.ContentLength);
                dynamic latest = JsonConvert.DeserializeObject(outputData);
                returnUrl = latest["NavigateURL"].Value;
            }
            response.Close();
            data.Close();
            streamRead.Close();

            return returnUrl;
        }

        public string GetTransaction(string transactionToken)
        {
            string transactionRefNo = "";
            var auth = 
              System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes($"{ ConfigSettings.PolipaymentApiMerchantCode }:{ ConfigSettings.PolipaymentApiAuthCode }"));

            var myRequest =
                System.Net.WebRequest.Create
                ($"{ _baseUrl}api/v2/Transaction/GetTransaction?token=" + HttpUtility.UrlEncode(transactionToken));
            myRequest.Method = "GET";
            myRequest.Headers.Add("Authorization", "Basic " + auth);

            var response = (System.Net.HttpWebResponse)myRequest.GetResponse();
            var data = response.GetResponseStream();
            var streamRead = new StreamReader(data);
            Char[] readBuff = new Char[response.ContentLength];
            int count = streamRead.Read(readBuff, 0, (int)response.ContentLength);
            while (count > 0)
            {
                var outputData = new String(readBuff, 0, count);
                Console.Write(outputData);
                count = streamRead.Read(readBuff, 0, (int)response.ContentLength);
                dynamic latest = JsonConvert.DeserializeObject(outputData);
                transactionRefNo = latest["TransactionRefNo"];
            }
            response.Close();
            data.Close();
            streamRead.Close();

            return transactionRefNo;
        }
    }
}