﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace AlphaPointPayment.Common.Utils
{
    //public static class EmailSender
    //{
    //    public static void SendEmail(
    //        ICollection<EmailAddressInfo> emailsTo,
    //        ICollection<EmailAddressInfo> emailsCC, 
    //        Dictionary<string, byte[]> attachments, 
    //        string subject, 
    //        string bodyHtml)
    //    {
    //        if(emailsTo == null || emailsTo.Count == 0)
    //            throw new ArgumentNullException("emailsTo");

    //        string[] notInWhiteListEmails = emailsTo.Union(emailsCC ?? new List<EmailAddressInfo>())
    //            .Where(m => !ConfigSettings.IsEmailAddressAllowed(m.Email))
    //            .Select(m => m.Email).ToArray();
    //        if(notInWhiteListEmails.Length > 0)
    //        {
    //            throw new Exception(String.Format("E-mail addresses is not allowed to send - \"{0}\"", String.Join(", ", notInWhiteListEmails)));
    //        }

    //        using (MailMessage letter = new MailMessage())
    //        {
    //            foreach (var l_addr in emailsTo)
    //            {
    //                letter.To.Add(l_addr.ToMailAddress());
    //            }

    //            if (emailsCC != null)
    //            {
    //                foreach (var l_addr in emailsCC)
    //                {
    //                    letter.CC.Add(l_addr.ToMailAddress());
    //                }
    //            }

    //            letter.Subject = subject;
    //            letter.IsBodyHtml = true;

    //            if (attachments != null)
    //            {
    //                foreach (KeyValuePair<string, byte[]> attachment in attachments)
    //                    letter.Attachments.Add(new System.Net.Mail.Attachment(new MemoryStream(attachment.Value), attachment.Key));
    //            }

    //            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(bodyHtml, null, "text/html");
    //            letter.AlternateViews.Add(htmlView);

    //            using(SmtpClient smtp = new SmtpClient())
    //            {
    //                smtp.Send(letter);
    //            }
    //        }
    //    }
    //}

    //public class EmailAddressInfo
    //{
    //    public string Email { get; private set; }
    //    public string DisplayName { get; private set; }

    //    public EmailAddressInfo(string email, string displayName = null)
    //    {
    //        if(String.IsNullOrWhiteSpace(email))
    //        {
    //            throw new ArgumentNullException("email");
    //        }
    //        if(!ValidateEmail(email))
    //        {
    //            throw new Exception(String.Format("The specified string is not in the form required for an e-mail address - \"{0}\"", email));
    //        }
    //        Email = email;
    //        DisplayName = displayName;
    //    }

    //    public MailAddress ToMailAddress()
    //    {
    //        return new MailAddress(Email, DisplayName);
    //    }

    //    public static bool ValidateEmail(string p_Email)
    //    {
    //        if (String.IsNullOrEmpty(p_Email))
    //            return false;

    //        Regex r = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
    //        Match m = r.Match(p_Email.Trim());
    //        return m.Success;
    //    }

    //    public static bool ValidateEmailList(String p_Email)
    //    {
    //        if (String.IsNullOrEmpty(p_Email))
    //            return false;

    //        Regex r = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(,\s*\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*$");
    //        Match m = r.Match(p_Email.Trim());
    //        return m.Success;
    //    }
    //}
}
