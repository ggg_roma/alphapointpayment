﻿using NLog;

namespace AlphaPointPayment.Common
{
    /// <summary>
    /// Summary description for LogHolder
    /// </summary>
    public class LogHolder
    {
        private readonly static Logger _MainLog = LogManager.GetLogger("MainLog");
        public static Logger MainLog
        {
            get { return _MainLog; }
        }

        private readonly static Logger _Http404Log = LogManager.GetLogger("Http404Log");
        public static Logger Http404Log
        {
            get { return _Http404Log; }
        }

        private readonly static Logger _PaymentLog = LogManager.GetLogger("PaymentLog");
        public static Logger PaymentLog
        {
            get { return _PaymentLog; }
        }
    }
}