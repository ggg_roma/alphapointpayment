﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using AlphaPointPayment.Common.Utils;

namespace AlphaPointPayment.Common
{
    public static class ConfigSettings
    {
        //public static bool ShowFullError
        //{
        //    get
        //    {
        //        return String.Equals("true", ConfigurationManager.AppSettings.Get("ShowFullError"),
        //                             StringComparison.OrdinalIgnoreCase);
        //    }
        //}

        //public static string AttachmentsFolder
        //{
        //    get
        //    {
        //        string path = ConfigurationManager.AppSettings.Get("AttachmentsFolder");
        //        return String.IsNullOrWhiteSpace(path) 
        //            ? String.Empty 
        //            : PathConverter.Current.FromRelativeToAbsolute(path);
        //    }
        //}

        //public static HashSet<string> AllowedEmailAddresses
        //{
        //    get
        //    {
        //        HashSet<string> l_set = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        //        string l_addr = ConfigurationManager.AppSettings.Get("AllowedEmailAddresses");
        //        if (!String.IsNullOrEmpty(l_addr))
        //        {
        //            string[] l_arr = l_addr.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
        //                .Select(m => m.Trim())
        //                .Where(m => !String.IsNullOrWhiteSpace(m)).ToArray();
        //            l_set.UnionWith(l_arr);
        //        }
        //        return l_set;
        //    }
        //}

        //public static bool IsEmailAddressAllowed(String p_emailAddressList)
        //{
        //    bool l_res = true;
        //    if (!String.IsNullOrWhiteSpace(p_emailAddressList))
        //    {
        //        HashSet<string> l_addressesAllowed = AllowedEmailAddresses;

        //        if (l_addressesAllowed.Count > 0)
        //        {
        //            String[] l_addresses = p_emailAddressList.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        //            foreach (string l_address in l_addresses)
        //            {
        //                if (!l_addressesAllowed.Contains(l_address))
        //                {
        //                    l_res = false;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    return l_res;
        //}

        //public static bool IsTraceEnabled
        //{
        //    get
        //    {
        //        var section = ConfigurationManager.GetSection("system.web/trace") as ConfigurationSection;
        //        if (section != null)
        //        {
        //            var prop = section.ElementInformation.Properties["enabled"];
        //            if (prop != null)
        //            {
        //                return String.Equals("true", prop.Value.ToString(),
        //                                 StringComparison.OrdinalIgnoreCase);
        //            }
        //        }
        //        return false;
        //    }
        //}

        //public static Enums.DbDataInitStrategyTypes DbDataInitStrategyType
        //{
        //    get
        //    {
        //        var rawValue = ConfigurationManager.AppSettings.Get("DbDataInitStrategyType");
        //        if (string.IsNullOrWhiteSpace(rawValue))
        //            return Enums.DbDataInitStrategyTypes.MigrateValidate;

        //        return (Enums.DbDataInitStrategyTypes)Enum.Parse(typeof(Enums.DbDataInitStrategyTypes), rawValue, true);
        //    }
        //}

        public static string TrustPayBaseUrl => ConfigurationManager.AppSettings.Get("TrustPayBaseUrl");
        public static string TrustPayUserId => ConfigurationManager.AppSettings.Get("TrustPayUserId");
        public static string TrustPayPassword => ConfigurationManager.AppSettings.Get("TrustPayPassword");
        public static string TrustPayEntityId => ConfigurationManager.AppSettings.Get("TrustPayEntityId");

        public static string RazorPayKey_id => ConfigurationManager.AppSettings.Get("RazorPayKey_id");
        public static string RazorPayKey_secret => ConfigurationManager.AppSettings.Get("RazorPayKey_secret");
        public static string RazorpayCheckoutScript => ConfigurationManager.AppSettings.Get("RazorpayCheckoutScript");
        public static string RazorPayFormCompanyLogoUrl => ConfigurationManager.AppSettings.Get("RazorPayFormCompanyLogoUrl");

        public static string AlphaPointAPI_Url => ConfigurationManager.AppSettings.Get("AlphaPointAPI_Url");

        public static string PolipaymentApiBaseUrl => ConfigurationManager.AppSettings.Get("PolipaymentApiBaseUrl");
        public static string PolipaymentApiAuthCode => ConfigurationManager.AppSettings.Get("PolipaymentApiAuthCode");
        public static string PolipaymentApiMerchantReference => ConfigurationManager.AppSettings.Get("PolipaymentApiMerchantReference");
        public static string PolipaymentApiMerchantCode => ConfigurationManager.AppSettings.Get("PolipaymentApiMerchantCode");
        public static string PolipaymentApiMerchantHomepageURL => ConfigurationManager.AppSettings.Get("PolipaymentApiMerchantHomepageURL");
        public static string PolipaymentApiSuccessURL => ConfigurationManager.AppSettings.Get("PolipaymentApiSuccessURL");
        public static string PolipaymentApiFailureURL => ConfigurationManager.AppSettings.Get("PolipaymentApiFailureURL");
        public static string PolipaymentApiCancellationURL => ConfigurationManager.AppSettings.Get("PolipaymentApiCancellationURL");
        public static string PolipaymentApiNotificationURL => ConfigurationManager.AppSettings.Get("PolipaymentApiNotificationURL");

        public static bool PSiGateUseSendbox => Convert.ToBoolean(ConfigurationManager.AppSettings.Get("PSiGateUseSendbox"));
        public static string PSiGateStoreID => ConfigurationManager.AppSettings.Get("PSiGateStoreID");
        public static string PSiGatePassphrase => ConfigurationManager.AppSettings.Get("PSiGatePassphrase");
        public static string PSiGateAPIUrlProductionEnvironment => ConfigurationManager.AppSettings.Get("PSiGateAPIUrlProductionEnvironment");
        public static string PSiGateAPIUrlTestEnvironment => ConfigurationManager.AppSettings.Get("PSiGateAPIUrlTestEnvironment");
        public static string PSiGateAPIUrl => ConfigurationManager.AppSettings.Get("PSiGateAPIUrl");

    }
}
