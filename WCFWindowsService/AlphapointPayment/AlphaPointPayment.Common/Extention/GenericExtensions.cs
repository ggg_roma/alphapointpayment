﻿using System;
using System.Linq.Expressions;
using AlphaPointPayment.Common.Utils;

namespace AlphaPointPayment.Common.Extensions
{
    public static class GenericExtensions
    {
        public static string GetPropertyName<TObject>(this TObject type,
                                                       Expression<Func<TObject, object>> propertyRefExpr)
        {
            return PropertyUtil.GetName(propertyRefExpr);
        }
    }
}
