﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace AlphaPointPayment.Common
{
    public class Enums
    {
        public enum NotificationEmailTypes
        {
            ResetPassword = 1
        }

        public enum MenuItemTypes
        {
            
        }

        public enum DbDataInitStrategyTypes
        {
            MigrateValidate,
            MigrateToLatest,
            None
        }

        public enum SchedulerOnDateShiftTypes
        {
            //TODO: implement
        }

        [DataContract]
        public enum Currencies
        {
            [Description("US Dollar")]
            [EnumMember(Value = "USD")]
            USD = 1,
            [Description("Canadian Dollar")]
            [EnumMember(Value = "CAD")]
            CAD,
            [Description("Euro")]
            [EnumMember(Value = "EUR")]
            EUR
        }

        public enum PaymentStatuses
        {
            New = 1,
            Submitted,
            Error,
            WrongComparedData
        }

        public enum TrustPayPaymentTypes
        {
            /// <summary>
            /// Debit: Debits the account of the end customer and credits the merchant account.
            /// </summary>
            DB = 1,
            /// <summary>
            /// Preauthorization: A stand-alone authorisation that will also trigger optional risk management and validation. 
            /// A Capture (CP) with reference to the Preauthorisation (PA) will confirm the payment.
            /// </summary>
            PA,
            /// <summary>
            /// Credit: Credits the account of the end customer and debits the merchant account.
            /// </summary>
            CD,
            /// <summary>
            /// Capture: Captures a preauthorized (PA) amount.
            /// </summary>
            CP,
            /// <summary>
            /// Reversal: Reverses an already processed Preauthorization (PA), Debit (DB) or Credit (CD) transaction. 
            /// As a consequence, the end customer will never see any booking on his statement. 
            /// A Reversal is only possible until a connector specific cut-off time. Some connectors don't support Reversals.
            /// </summary>
            RV,
            /// <summary>
            /// Refund: Credits the account of the end customer with a reference to a prior Debit (DB) or Credit (CD) transaction. 
            /// The end customer will always see two bookings on his statement. Some connectors do not support Refunds.
            /// </summary>
            RF
        }

        public enum ApDepositStatus
        {
            New = 1,
            AdminProcessing,
            Accepted,
            Rejected,
            SystemProcessing,
            FullyProcessed,
            Failed,
            Pending
        }

        /// <summary>
        /// Represents a payment status enumeration of PCiGate
        /// </summary>
        public enum PaymentStatusPCiGate : int
        {
            /// <summary>
            /// Pending
            /// </summary>
            Pending = 10,
            /// <summary>
            /// Authorized
            /// </summary>
            Authorized = 20,
            /// <summary>
            /// Paid
            /// </summary>
            Paid = 30,
            /// <summary>
            /// Partially Refunded
            /// </summary>
            PartiallyRefunded = 35,
            /// <summary>
            /// Refunded
            /// </summary>
            Refunded = 40,
            /// <summary>
            /// Voided
            /// </summary>
            Voided = 50,
        }

    }
}
