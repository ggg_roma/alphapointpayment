﻿namespace AlphaPointPayment.API.Models.ResponseModels
{
    public class WebAuthenticateUserResponse
    {
        public bool Authenticated { get; set; }
        public string SessionToken { get; set; }
        public int? UserId { get; set; }
    }
}
