﻿namespace AlphaPointPayment.API.Models.ResponseModels
{
    public class UpdateDepositTicketResponse
    {
        public bool result { get; set; }
        public string errormsg { get; set; }
        public int errorcode { get; set; }
        public string detail { get; set; }
    }
}
