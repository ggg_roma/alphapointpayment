﻿using AlphaPointPayment.API.Models.APIModels;

namespace AlphaPointPayment.API.Models.ResponseModels
{
    public class AuthenticateUserResponse
    {
        public bool Authenticated { get; set; }
        public AlphaPointUser User { get; set; }
    }
}
