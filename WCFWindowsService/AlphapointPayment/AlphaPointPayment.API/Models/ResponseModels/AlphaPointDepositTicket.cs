﻿using System.Collections.Generic;

namespace AlphaPointPayment.API.Models.ResponseModels
{
    public class AlphaPointDepositTicket
    {
        public int AssetManagerId { get; set; }
        public int AccountId { get; set; }
        public int AssetId { get; set; }
        public string AssetName { get; set; }
        public decimal Amount { get; set; }
        public int OMSId { get; set; }
        public string RequestCode { get; set; }
        public string RequestIP { get; set; }
        public int RequestUser { get; set; }
        public string RequestUserName { get; set; }
        public int OperatorId { get; set; }
        public string Status { get; set; }
        public decimal FeeAmt { get; set; }
        public int UpdatedByUser { get; set; }
        public string UpdatedByUserName { get; set; }
        public int TicketNumber { get; set; }
        public string DepositInfo { get; set; }
        public string CreatedTimestamp { get; set; }
        public string LastUpdateTimeStamp { get; set; }
        public ICollection<string> Comments { get; set; }
        public ICollection<string> Attachments { get; set; }
    }
}
