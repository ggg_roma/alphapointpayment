﻿using System;

namespace AlphaPointPayment.API.Models.ParametersModels
{
    public class GetAllDepositTicketsParametersModel
    {
        public GetAllDepositTicketsParametersModel(int? AccountId = null, string Status = null, int? TicketId = null, int? Limit = null,
            string Username = null, DateTime? StartTimestamp = null, DateTime? EndTimestamp = null, decimal? Amount = null, int? AmountOperator = null)
        {
            if (Amount != null && AmountOperator == null) throw new ArgumentException("You can not use the 'Amount' option without the 'AmountOperator'");
            if (AmountOperator != null && AmountOperator != 0 && AmountOperator != 1 && Amount != 2) throw new ArgumentException("AmountOperator can be only: 0; 1; 2; or null");
            this.AccountId = AccountId;
            this.Status = Status;
            this.TicketId = TicketId;
            this.Limit = Limit;
            this.Username = Username;
            this.StartTimestamp = StartTimestamp;
            this.EndTimestamp = EndTimestamp;
            this.Amount = Amount;
            this.AmountOperator = AmountOperator;
        }

        public int OMSId { get; set; }
        public int Operatorid { get; set; }
       
        // optional
        public int? AccountId { get; set; }
       
        // optional
        public string Status { get; set; }
       
        // optional
        public int? TicketId { get; set; }
        
        // optional
        public int? Limit { get; set; }
        
        // optional
        public string Username { get; set; }
       
        // optional
        public DateTime? StartTimestamp { get; set; }
        
        // optional
        public DateTime? EndTimestamp { get; set; }
        
        // optional
        public decimal? Amount { get; set; }
       
        // optional - required with amount. Values are 0: "=", 1: "amount>=value", 2: "amount<=value"
        public int? AmountOperator { get; set; }
    }

}
