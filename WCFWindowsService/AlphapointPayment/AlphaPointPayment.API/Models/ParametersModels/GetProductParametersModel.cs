﻿namespace AlphaPointPayment.API.Models.ParametersModels
{
    public class GetProductParametersModel
    {
        public int OMSId { get; set; }
        public int ProductId { get; set; }
    }
}
