﻿namespace AlphaPointPayment.API.Models.ParametersModels
{
    public class GetDepositTicketParametersModel
    {
        public int OMSId { get; set; }
        public int AccountId { get; set; }
        public string RequestCode { get; set; }
        public int? TicketId { get; set; }
    }
}
