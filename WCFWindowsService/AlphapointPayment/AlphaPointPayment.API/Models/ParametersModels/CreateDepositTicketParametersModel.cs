﻿using AlphaPointPayment.API.Models.ResponseModels;

namespace AlphaPointPayment.API.Models.ParametersModels
{
    public class CreateDepositTicketParametersModel
    {
        public int OperatorId { get; set; }
        public int AccountId { get; set; }
        public int AssetId { get; set; }
        public decimal Amount { get; set; }
        public int OMSId { get; set; }
        public string DepositInfo { get; set; }
    }
}
