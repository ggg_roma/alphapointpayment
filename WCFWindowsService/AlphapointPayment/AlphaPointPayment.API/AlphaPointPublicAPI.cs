﻿using AlphaPointPayment.API.Models;
using AlphaPointPayment.API.Models.ParametersModels;
using AlphaPointPayment.API.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using AlphaPointPayment.Common;
using WebSocketSharp;

namespace AlphaPointPayment.API
{
    public class AlphaPointPublicAPI : IDisposable
    {
        #region Properties
        private string AlphaPointSocket { get; set; }
        private int SequenceNumber { get; set; }
        private WebSocket Ws { get; set; }
        private bool IsAuth { get; set; }
        TaskCompletionSource<string> Tcs { get; set; }
        #endregion

        #region Consructors
        /// <summary>
        /// Initialize Socket address by using string APISocketAddress
        /// </summary>
        /// <param name="apiSocketAddress">Address of AlphaPoint WebSocket</param>
        public AlphaPointPublicAPI(string apiSocketAddress)
        {
            AlphaPointSocket = apiSocketAddress;
            SequenceNumber = 0;
            Ws = new WebSocket(AlphaPointSocket);
            Ws.Connect();
            IsAuth = false;
            Ws.OnMessage += (sender, e) =>
            {
                Tcs.SetResult(e.Data);
            };
        }

        /// <summary>
        /// Initialize Socket
        /// </summary>
        /// <param name="apiSocketAddress">Address of AlphaPoint WebSocket<</param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public AlphaPointPublicAPI(string apiSocketAddress, string userName, string password)
        {
            AlphaPointSocket = apiSocketAddress;
            //this.UserName = UserName;
            //this.Password = Password;
            Ws = new WebSocket(AlphaPointSocket);
            Ws.Connect();
            Ws.OnMessage += (sender, e) =>
            {
                Tcs.SetResult(e.Data);
            };
            SequenceNumber = 0;
            IsAuth = AuthenticateUser(userName, password).Authenticated;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Function that returns the sequence number of the message. The Client-Side sequence number should always be an Even Number, 
        /// such that your sequence number variable should always be incremented by 2 (i += 2). 
        /// All messages will contain either an original sequence number (message types 0, 2, 3, 4), 
        /// or will contain the ID of the request message the message is in reply to (message types 1, 5).
        /// </summary>
        /// <returns></returns>
        private int GetI()
        {
            var oldI = SequenceNumber;
            SequenceNumber += 2;
            return oldI;
        }

        /// <summary>
        /// Sends request to WebSocket API
        /// </summary>
        /// <param name="frame">All WebSocket Calls and Replies are embedded into a JSON-Formatted Frame object containing the relevant information about the call or reply, as well as the payload.</param>
        /// <returns>Returns Frame with API response data</returns>
        private string SendRequest(Frame frame)
        {
            try
            {
                using (var ws = new WebSocketSharp.WebSocket(AlphaPointSocket))
                {
                    TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();
                    ws.OnMessage += (sender, e) =>
                    {
                        tcs.SetResult(e.Data);
                    };
                    ws.Connect();
                    ws.Send(new JavaScriptSerializer().Serialize(frame));
                    return tcs.Task.Result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string SendNewRequest(Frame frame)
        {
            try
            {
                Tcs = new TaskCompletionSource<string>();

                Ws.Send(new JavaScriptSerializer().Serialize(frame));
                return Tcs.Task.Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Forming request to API and handling response from API
        /// </summary>
        /// <typeparam name="T">Type of entity to handle response object</typeparam>
        /// <param name="requestFrame">Frame object containing the relevant information about the call or reply</param>
        /// <param name="parametersObject">Parameters object containing the data being sent with the message</param>
        /// <returns>Returns list of T entities</returns>
        private List<T> GetResponseList<T>(Frame requestFrame, object parametersObject)
        {
            List<T> resultList = new List<T>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            requestFrame.o = serializer.Serialize(parametersObject);

            string response = SendNewRequest(requestFrame);
            Frame result = serializer.Deserialize<Frame>(response);
            resultList = serializer.Deserialize<List<T>>(result.o);
            return resultList;
        }

        /// <summary>
        /// Forming request to API and handling response from API
        /// </summary>
        /// <typeparam name="T">Type of entity to handle response object</typeparam>
        /// <param name="requestFrame">Frame object containing the relevant information about the call or reply</param>
        /// <param name="parametersObject">Parameters object containing the data being sent with the message</param>
        /// <returns>Returns T entity</returns>
        private T GetResponseSingle<T>(Frame requestFrame, object parametersObject)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            requestFrame.o = serializer.Serialize(parametersObject);

            string response = SendNewRequest(requestFrame);
            Frame result = serializer.Deserialize<Frame>(response);
            T resultEntity = serializer.Deserialize<T>(result.o);
            return resultEntity;
        }

        /// <summary>
        /// Serialize object using JavaScriptSerializer
        /// </summary>
        /// <param name="o">Object to serialize</param>
        /// <returns></returns>
        private string SerializeObject(object o)
        {
            return new JavaScriptSerializer().Serialize(o);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="remoteFunction">Function name that will be called API</param>
        /// <returns></returns>
        private Frame GetRequestFrame(string remoteFunction)
        {
            return new Frame() { m = 0, i = 0, n = remoteFunction };
        }

        ////Signature is a HMAC-SHA256 encoded message containing: nonce, API Key and Client ID. The HMAC-SHA256 code must be generated using your API Secret Key.
        //This code must be converted to it's hexadecimal representation (64 uppercase characters).
        private string GenerateSignature(string apiKey, string apiSecretKey, int clientId, int nonce)
        {
            var secret = apiSecretKey ?? "";
            var message = nonce + apiKey + clientId;
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return BitConverter.ToString(hashmessage).Replace("-", "");
            }
        }

        /// <summary>
        /// Get required deposit info to perform a deposit
        /// </summary>
        /// <param name="accountId">User account ID</param>
        /// <param name="productId">ID of deposite product</param>
        /// <returns></returns>
        private AlphaPointDepositInfo GetDepositInfo(int accountId, int productId)
        {
            AlphaPointDepositInfo result = new AlphaPointDepositInfo();
            try
            {
                //get deposit info
                Frame f = GetRequestFrame("GetDepositInfo");
                GetDepositInfoParametersModel model = new GetDepositInfoParametersModel() { OMSId = 1, AccountId = accountId, ProductId = productId };
                result = GetResponseSingle<AlphaPointDepositInfo>(f, model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Get a matching deposit ticket by request code or ticket id
        /// </summary>
        /// <param name="accountId">User account Id</param>
        /// <param name="requestCode">Optional Guid string</param>
        /// <param name="ticketId">Optional ticket Id</param>
        /// <returns></returns>
        private AlphaPointDepositTicket GetDepositTicket(int accountId, string requestCode = null, int? ticketId = null)
        {
            AlphaPointDepositTicket result = new AlphaPointDepositTicket();
            try
            {
                Frame f = GetRequestFrame("GetDepositTicket");
                GetDepositTicketParametersModel parameters = new GetDepositTicketParametersModel() { OMSId = 1, AccountId = accountId, RequestCode = requestCode, TicketId = ticketId };
                result = GetResponseSingle<AlphaPointDepositTicket>(f, parameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Create a deposit ticket to be tracked by AlphaPoint ticketing system
        /// </summary>
        /// <param name="accountId">User account ID</param>
        /// <param name="productId">ID of deposit product</param>
        /// <param name="amount">Ticket amount</param>
        /// <returns></returns>
        public AlphaPointCreateDepositTicketResponseModel CreateDepositTicket(int accountId, int productId, decimal amount)
        {
            if (!IsAuth) throw new InvalidOperationException("You must authenticate user before call this method. Please call AuthenticateUser method before call current method.");

            AlphaPointCreateDepositTicketResponseModel response = new AlphaPointCreateDepositTicketResponseModel();
            response.success = false;
            response.requestcode = "Bad Request";

            try
            {
                //get deposit info
                var info = GetDepositInfo(accountId, productId);

                Frame f = GetRequestFrame("CreateDepositTicket");
                CreateDepositTicketParametersModel dtModel = new CreateDepositTicketParametersModel()
                { AccountId = accountId, OMSId = 1, Amount = amount, AssetId = info.AssetId, OperatorId = 1, DepositInfo = SerializeObject(info.DepositInfo) };
                response = GetResponseSingle<AlphaPointCreateDepositTicketResponseModel>(f, dtModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        /// <summary>
        /// Update a deposit ticket - typically status or amount
        /// </summary>
        /// <param name="accountId">User account Id</param>
        /// <param name="status">Status for update</param>
        /// <param name="amount">Amount for update</param>
        /// <param name="requestCode">Optional Guid string</param>
        /// <param name="ticketId">Optional ticket Id</param>
        public UpdateDepositTicketResponse UpdateDepositTicket(int accountId, Enums.ApDepositStatus? status, decimal? amount = null, string requestCode = null, int? ticketId = null)
        {
            if (!IsAuth) throw new InvalidOperationException("You must authenticate user before call this method. Please call AuthenticateUser method before call current method.");

            UpdateDepositTicketResponse result = new UpdateDepositTicketResponse();
            try
            {
                var oldDeposit = GetDepositTicket(accountId, requestCode, ticketId);
                oldDeposit.Amount = amount ?? oldDeposit.Amount;
                if (status.HasValue)
                {
                    oldDeposit.Status = status.ToString();
                }
                Frame f = GetRequestFrame("UpdateDepositTicket");
                AlphaPointDepositTicket deposit = new AlphaPointDepositTicket();
                deposit = oldDeposit;
                result = GetResponseSingle<UpdateDepositTicketResponse>(f, deposit);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Authentication methods
        /// <summary>
        /// Authentificate user connection to WebSocket
        /// </summary>
        /// <param name="userName">User login</param>
        /// <param name="password">User password</param>
        /// <returns></returns>
        public AuthenticateUserResponse AuthenticateUser(string userName, string password)
        {
            try
            {
                Frame f = GetRequestFrame("AuthenticateUser");
                UserNamePasswordAuthModel model = new UserNamePasswordAuthModel() { UserName = userName, Password = password };
                var result = GetResponseSingle<AuthenticateUserResponse>(f, model);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// End the current session
        /// </summary>
        /// <returns></returns>
        public bool LogOut()
        {
            bool result = false;
            try
            {
                Frame f = GetRequestFrame("LogOut");
                result = GetResponseSingle<StandardAPIResponse>(f, "{}").result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    LogOut();
                    Ws.Close();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
