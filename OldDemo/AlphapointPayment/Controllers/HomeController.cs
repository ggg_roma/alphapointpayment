﻿using AlphaPointPayment.API;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AlphapointPayment.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return RedirectToAction("Index", "TrustPay");
        }

        public ActionResult TestAlphaPointAPICall()
        {
            var products = AlphaPointPublicAPI.UnAuthenticatedCalls.GetProducts();
            return View(products);
        }
    }
}
