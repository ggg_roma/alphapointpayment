﻿using AlphapointPayment.Models.TrustPay;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace AlphapointPayment.Controllers
{
    public class TrustPayController : Controller
    {
        // GET: TrustPay
        public ActionResult Index()
        {
            return View();
        }        

        [HttpPost]
        public ActionResult PrepareCheckout(PrepareCheckoutModel model)
        {
            var responseData = PrepareCheckoutRequest(model);
            var checkoutId = responseData["id"];

            var rezModel = new PrepareCheckoutOutput(checkoutId);
            return Json(rezModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPrePaymentInfo(PrePaymentInfoViewModel model)
        {
            return PartialView("_PrePaymentInfo", model);
        }

        private Dictionary<string, dynamic> PrepareCheckoutRequest(PrepareCheckoutModel model)
        {
            Dictionary<string, dynamic> responseData;
            string data = "authentication.userId=8a8294174e735d0c014e78e083bc197b" +
                "&authentication.password=K4ceFFSmYA" +
                "&authentication.entityId=8a8294174e735d0c014e78e0839f1977" +
                $"&amount={model.Amount}" +
                $"&currency={model.Currency}" +
                "&paymentType=DB";
            string url = "https://test.oppwa.com/v1/checkouts";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            Stream PostData = request.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                var s = new JavaScriptSerializer();
                responseData = s.Deserialize<Dictionary<string, dynamic>>(reader.ReadToEnd());
                reader.Close();
                dataStream.Close();
            }
            return responseData;
        }
    }
}