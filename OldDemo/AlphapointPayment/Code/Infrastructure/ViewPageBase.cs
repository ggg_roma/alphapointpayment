﻿namespace AlphapointPayment.Code.Infrastructure
{
    public abstract class ViewPageBase<T> : System.Web.Mvc.WebViewPage<T>
    {
        protected ViewDataItems DataItems { get; private set; }

        protected ViewPageBase()
        {
            DataItems = new ViewDataItems();
        }
    }
}