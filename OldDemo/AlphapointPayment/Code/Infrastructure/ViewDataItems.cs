﻿using AlphapointPayment.Common;
using AlphapointPayment.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AlphapointPayment.Code.Infrastructure
{
    public class ViewDataItems
    {
        private Dictionary<string, object> Cache { get; set; }

        public ViewDataItems()
        {           
            Cache = new Dictionary<string, object>();
        }

        public List<SelectListItem> Currencies
        {
            get
            {
                return Get("Currencies",
                    () => EnumExtensions.EnumToList<Enums.Currencies>()
                        .Select(m => new SelectListItem()
                        {
                            Text = m.Value,
                            Value = m.Key.ToString()
                        }).ToList());
            }
        }

        private T Get<T>(string key, Func<T> loadAction)
        {
            T res;
            Object obj;
            if (Cache.TryGetValue(key, out obj))
            {
                res = (T)obj;
            }
            else
            {
                res = loadAction();
                Cache.Add(key, res);
            }
            return res;
        }
    }
}