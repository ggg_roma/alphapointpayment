﻿using System.ComponentModel;

namespace AlphapointPayment.Common
{
    public class Enums
    {
        public enum Currencies
        {
            [Description("USD")]
            USD = 1,
            [Description("Euro")]
            EUR
        }
    }
}
