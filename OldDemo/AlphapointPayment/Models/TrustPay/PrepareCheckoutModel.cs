﻿using AlphapointPayment.Common;
using System.ComponentModel.DataAnnotations;

namespace AlphapointPayment.Models.TrustPay
{
    public class PrepareCheckoutModel
    {
        public decimal Amount { get; set; }

        [Display(Name = "AccountId(for testing, need to know how to get AccountId)")]
        public int AccountId { get; set; }

        public Enums.Currencies Currency { get; set; }
    }
}