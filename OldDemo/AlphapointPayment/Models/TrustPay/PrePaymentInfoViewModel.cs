﻿using AlphapointPayment.Common;

namespace AlphapointPayment.Models.TrustPay
{
    public class PrePaymentInfoViewModel
    {
        public decimal Amount { get; set; }

        public Enums.Currencies Currency { get; set; }
    }
}