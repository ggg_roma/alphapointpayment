﻿namespace AlphapointPayment.Models.TrustPay
{
    public class PrepareCheckoutOutput
    {
        public PrepareCheckoutOutput(string checkoutId)
        {
            CheckoutId = checkoutId;
        }

        public string CheckoutId { get; set; }

        public string ScriptSrc => $"https://test.oppwa.com/v1/paymentWidgets.js?checkoutId={CheckoutId}";
    }
}