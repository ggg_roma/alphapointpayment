﻿namespace AlphaPointPayment.API.Models
{
    /// <summary>
    /// Class contains a information about the call or reply, as well as the payload.
    /// </summary>
    public class Frame
    {
        public int m { get; set; }
        public int i { get; set; }
        public string n { get; set; }
        public string o { get; set; }
    }
}
