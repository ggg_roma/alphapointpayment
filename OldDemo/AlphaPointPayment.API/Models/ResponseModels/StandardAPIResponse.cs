﻿namespace AlphaPointPayment.API.Models.ResponseModels
{
    /// <summary>
    /// Class represent API response, that not returning object data or common error response
    /// </summary>
    class StandardAPIResponse
    {
        public bool result { get; set; }
        public string errormsg { get; set; }
        public int errorcode { get; set; }
        public string detail { get; set; }
    }
}
