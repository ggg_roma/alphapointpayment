﻿namespace AlphaPointPayment.API.Models.ResponseModels
{
    public class AlphaPointInstrument
    {
        public int OMSId { get; set; }
        public int InstrumentId { get; set; }
        public string Symbol { get; set; }
        public int Product1 { get; set; }
        public string Product1Symbol { get; set; }
        public int Product2 { get; set; }
        public string Product2Symbol { get; set; }
        public string InstrumentType { get; set; }
        public int VenueInstrumentId { get; set; }
        public int SortIndex { get; set; }
    }
}
