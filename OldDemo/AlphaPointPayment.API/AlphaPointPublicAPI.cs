﻿using AlphaPointPayment.API.Models;
using AlphaPointPayment.API.Models.ParametersModels;
using AlphaPointPayment.API.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AlphaPointPayment.API
{
    public static class AlphaPointPublicAPI
    {
        private static string AlphaPointSocket { get; set; }

        #region ctors
        /// <summary>
        /// Initialize Socket address
        /// </summary>
        static AlphaPointPublicAPI()
        {
            AlphaPointSocket = "ws://api.qa1.alphapoint.com:8086/WSGateway/";
        }
        #endregion

        #region private methods
        /// <summary>
        /// Sends request to WebSocket API
        /// </summary>
        /// <param name="frame">All WebSocket Calls and Replies are embedded into a JSON-Formatted Frame object containing the relevant information about the call or reply, as well as the payload.</param>
        /// <returns>Returns Frame with API response data</returns>
        private static string SendRequest(Frame frame)
        {
            try
            {
                using (var ws = new WebSocketSharp.WebSocket(AlphaPointSocket))
                {
                    TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();
                    ws.OnMessage += (sender, e) =>
                    {
                        tcs.SetResult(e.Data);
                    };
                    ws.Connect();
                    ws.Send(new JavaScriptSerializer().Serialize(frame));
                    return tcs.Task.Result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Forming request to API and handling response from API
        /// </summary>
        /// <typeparam name="T">Type of entity to handle response object</typeparam>
        /// <param name="requestFrame">Frame object containing the relevant information about the call or reply</param>
        /// <param name="parametersObject">Parameters object containing the data being sent with the message</param>
        /// <returns></returns>
        private static List<T> GetResponse<T>(Frame requestFrame, object parametersObject)
        {
            List<T> resultList = new List<T>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            requestFrame.o = serializer.Serialize(parametersObject);

            string response = SendRequest(requestFrame);
            Frame result = serializer.Deserialize<Frame>(response);
            resultList = serializer.Deserialize<List<T>>(result.o);
            return resultList;
        }
        #endregion


        public static class UnAuthenticatedCalls
        {
            /// <summary>
            /// Requests a list of available Products from the API.
            /// </summary>
            /// <returns></returns>
            public static List<AlphaPointProduct> GetProducts()
            {
                List<AlphaPointProduct> resultList = new List<AlphaPointProduct>();
                try
                {
                    Frame f = new Frame() { m = 0, i = 0, n = "GetProducts" };
                    GetProductsParametersModel parameters = new GetProductsParametersModel() { OMSId = 1 };
                    resultList = GetResponse<AlphaPointProduct>(f, parameters);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return resultList;
            }

            /// <summary>
            /// Requests a list of available trading instruments (pairs) from the API.
            /// </summary>
            /// <returns></returns>
            public static List<AlphaPointInstrument> GetInstruments()
            {
                List<AlphaPointInstrument> resultList = new List<AlphaPointInstrument>();
                try
                {
                    Frame f = new Frame() { m = 0, i = 0, n = "GetInstruments" };
                    GetInstrumentsParametersModel parameters = new GetInstrumentsParametersModel() { OMSId = 1 };
                    resultList = GetResponse<AlphaPointInstrument>(f, parameters);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return resultList;
            }
        }

        public static class AuthenticatedCalls
        {

        }

        public static class UserAccountCalls
        {

        }

        public static class UserInfoOrConfigCalls
        {

        }

        public static class OrderHandlingCalls
        {

        }

        public static class DepositsAndDepositTicketCalls
        {

        }

        public static class WithdrawAndWithdrawTicketCalls
        {

        }

        public static class TransferCalls
        {

        }
    }
}
