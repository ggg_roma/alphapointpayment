﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaPointPayment.API.Models.ParametersModels
{
    public class KeysAuthModel
    {
        public string APIKey { get; set; }
        public int Nonce { get; set; }
        public string Signature { get; set; }
    }
}
