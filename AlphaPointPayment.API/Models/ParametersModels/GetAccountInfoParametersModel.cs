﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlphaPointPayment.API.Models.ParametersModels
{
    public class GetAccountInfoParametersModel
    {
        public int OMSId { get; set; }
        public int AccountId { get; set; }
    }
}
