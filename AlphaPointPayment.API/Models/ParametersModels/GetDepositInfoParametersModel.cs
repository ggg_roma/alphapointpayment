﻿namespace AlphaPointPayment.API.Models.ParametersModels
{
    public class GetDepositInfoParametersModel
    {
        public int OMSId { get; set; }
        public int AccountId { get; set; }
        public int ProductId { get; set; }
    }
}
