﻿namespace AlphaPointPayment.API.Models.ParametersModels
{
    public class GetDepositTicketsParametersModel
    {
        public int OMSId { get; set; }
        public int AccountId { get; set; }
        public int Operatorid { get; set; }
    }
}
