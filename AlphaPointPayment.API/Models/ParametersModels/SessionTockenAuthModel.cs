﻿namespace AlphaPointPayment.API.Models.ParametersModels
{
    public class SessionTockenAuthModel
    {
        public string SessionToken { get; set; }
    }
}
