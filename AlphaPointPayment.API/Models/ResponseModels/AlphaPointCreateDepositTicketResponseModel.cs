﻿namespace AlphaPointPayment.API.Models.ResponseModels
{
    public class AlphaPointCreateDepositTicketResponseModel
    {
        public bool success { get; set; }
        public string requestcode { get; set; }
    }
}
