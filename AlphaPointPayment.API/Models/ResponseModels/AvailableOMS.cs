﻿namespace AlphaPointPayment.API.Models.ResponseModels
{
    public class AvailableOMS
    {
        public int OMSId { get; set; }
        public string OMSName { get; set; }
    }
}
