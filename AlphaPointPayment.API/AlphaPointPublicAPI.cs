﻿using AlphaPointPayment.API.Models;
using AlphaPointPayment.API.Models.ParametersModels;
using AlphaPointPayment.API.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AlphaPointPayment.API
{
    public class AlphaPointPublicAPI : IDisposable
    {
        private string AlphaPointSocket { get; set; }
        private string UserName { get; set; }
        private string Password { get; set; }
        private int sequenceNumber;

        #region ctors
        /// <summary>
        /// Initialize Socket address by using string APISocketAddress
        /// </summary>
        /// <param name="APISocketAddress">Address of AlphaPoint WebSocket</param>
        public AlphaPointPublicAPI(string APISocketAddress)
        {
            AlphaPointSocket = APISocketAddress;
            sequenceNumber = 0;
        }

        /// <summary>
        /// Initialize Socket
        /// </summary>
        /// <param name="APISocketAddress">Address of AlphaPoint WebSocket<</param>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        public AlphaPointPublicAPI(string APISocketAddress, string UserName, string Password)
        {
            AlphaPointSocket = APISocketAddress;
            this.UserName = UserName;
            this.Password = Password;
            sequenceNumber = 0;
        }
        #endregion

        #region private methods
        /// <summary>
        /// Function that returns the sequence number of the message. The Client-Side sequence number should always be an Even Number, 
        /// such that your sequence number variable should always be incremented by 2 (i += 2). 
        /// All messages will contain either an original sequence number (message types 0, 2, 3, 4), 
        /// or will contain the ID of the request message the message is in reply to (message types 1, 5).
        /// </summary>
        /// <returns></returns>
        private int GetI()
        {
            var oldI = sequenceNumber;
            sequenceNumber += 2;
            return oldI;
        }

        /// <summary>
        /// Sends request to WebSocket API
        /// </summary>
        /// <param name="frame">All WebSocket Calls and Replies are embedded into a JSON-Formatted Frame object containing the relevant information about the call or reply, as well as the payload.</param>
        /// <returns>Returns Frame with API response data</returns>
        private string SendRequest(Frame frame)
        {
            try
            {
                using (var ws = new WebSocketSharp.WebSocket(AlphaPointSocket))
                {
                    TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();
                    ws.OnMessage += (sender, e) =>
                    {
                        tcs.SetResult(e.Data);
                    };
                    ws.Connect();
                    ws.Send(new JavaScriptSerializer().Serialize(frame));
                    return tcs.Task.Result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Forming request to API and handling response from API
        /// </summary>
        /// <typeparam name="T">Type of entity to handle response object</typeparam>
        /// <param name="requestFrame">Frame object containing the relevant information about the call or reply</param>
        /// <param name="parametersObject">Parameters object containing the data being sent with the message</param>
        /// <returns>Returns list of T entities</returns>
        private List<T> GetResponseList<T>(Frame requestFrame, object parametersObject)
        {
            List<T> resultList = new List<T>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            requestFrame.o = serializer.Serialize(parametersObject);

            string response = SendRequest(requestFrame);
            Frame result = serializer.Deserialize<Frame>(response);
            resultList = serializer.Deserialize<List<T>>(result.o);
            return resultList;
        }

        /// <summary>
        /// Forming request to API and handling response from API
        /// </summary>
        /// <typeparam name="T">Type of entity to handle response object</typeparam>
        /// <param name="requestFrame">Frame object containing the relevant information about the call or reply</param>
        /// <param name="parametersObject">Parameters object containing the data being sent with the message</param>
        /// <returns>Returns T entity</returns>
        private T GetResponseSingle<T>(Frame requestFrame, object parametersObject)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            requestFrame.o = serializer.Serialize(parametersObject);

            string response = SendRequest(requestFrame);
            Frame result = serializer.Deserialize<Frame>(response);
            T resultEntity = serializer.Deserialize<T>(result.o);
            return resultEntity;
        }

        /// <summary>
        /// Serialize object using JavaScriptSerializer
        /// </summary>
        /// <param name="o">Object to serialize</param>
        /// <returns></returns>
        private string SerializeObject(object o)
        {
            return new JavaScriptSerializer().Serialize(o);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="remoteFunction">Function name that will be called API</param>
        /// <returns></returns>
        private Frame GetRequestFrame(string remoteFunction)
        {
            return new Frame() { m = 0, i = 0, n = remoteFunction };
        }

        ////Signature is a HMAC-SHA256 encoded message containing: nonce, API Key and Client ID. The HMAC-SHA256 code must be generated using your API Secret Key.
        //This code must be converted to it's hexadecimal representation (64 uppercase characters).
        private string GenerateSignature(string ApiKey, string ApiSecretKey, int ClientId, int nonce)
        {
            var secret = ApiSecretKey ?? "";
            var message = nonce + ApiKey + ClientId;
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return BitConverter.ToString(hashmessage).Replace("-", ""); //Convert.ToBase64String(hashmessage);
            }
        }
        #endregion

        #region public methods
        /// <summary>
        /// Authentificate user connection to WebSocket
        /// </summary>
        /// <param name="userName">User login</param>
        /// <param name="password">User password</param>
        /// <returns></returns>
        public AuthenticateUserResponse AuthenticateUser(string userName, string password)
        {
            try
            {
                Frame f = GetRequestFrame("AuthenticateUser");
                UserNamePasswordAuthModel model = new UserNamePasswordAuthModel() { UserName = userName, Password = password };
                var result = GetResponseSingle<AuthenticateUserResponse>(f, model);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// You must call this in order to use any of the authenticated calls.
        /// </summary>
        /// <param name="userName">User login from AlphaPoint System</param>
        /// <param name="password">User password from AlphaPoint System</param>
        /// <returns></returns>
        public WebAuthenticateUserResponse WebAuthenticateUser(string userName, string password)
        {
            try
            {
                Frame f = GetRequestFrame("WebAuthenticateUser");
                UserNamePasswordAuthModel model = new UserNamePasswordAuthModel() { UserName = userName, Password = password };
                var result = GetResponseSingle<WebAuthenticateUserResponse>(f, model);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// End the current session
        /// </summary>
        /// <returns></returns>
        public bool LogOut()
        {
            bool result = false;
            try
            {
                Frame f = GetRequestFrame("LogOut");
                result = GetResponseSingle<StandardAPIResponse>(f, "{}").result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Get required deposit info to perform a deposit
        /// </summary>
        /// <param name="accountId">User account ID</param>
        /// <param name="productId">ID of deposite product</param>
        /// <returns></returns>
        public AlphaPointDepositInfo GetDepositInfo(int accountId, int productId)
        {
            AlphaPointDepositInfo result = new AlphaPointDepositInfo();
            try
            {
                //get deposit info
                Frame f = GetRequestFrame("GetDepositInfo");
                GetDepositInfoParametersModel model = new GetDepositInfoParametersModel() { OMSId = 1, AccountId = accountId, ProductId = productId };
                result = GetResponseSingle<AlphaPointDepositInfo>(f, model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Create a deposit ticket to be tracked by AlphaPoint ticketing system
        /// </summary>
        /// <param name="accountId">User account ID</param>
        /// <param name="productId">ID of deposit product</param>
        /// <param name="amount">Ticket amount</param>
        /// <returns></returns>
        public AlphaPointCreateDepositTicketResponseModel CreateDepositTicket(int accountId, int productId, decimal amount)
        {
            AlphaPointCreateDepositTicketResponseModel response = new AlphaPointCreateDepositTicketResponseModel();
            response.success = false;
            response.requestcode = "Bad Request";

            try
            {
                //get deposit info
                var info = GetDepositInfo(accountId, productId);

                Frame f = GetRequestFrame("CreateDepositTicket");
                CreateDepositTicketParametersModel dtModel = new CreateDepositTicketParametersModel()
                { AccountId = accountId, OMSId = 1, Amount = amount, AssetId = info.AssetId, DepositInfo = SerializeObject(info) };
                response = GetResponseSingle<AlphaPointCreateDepositTicketResponseModel>(f, dtModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        /// <summary>
        /// Update a deposit ticket - typically status or amount
        /// </summary>
        /// <param name="AccountId">User account Id</param>
        /// <param name="status">Status for update</param>
        /// <param name="amount">Amount for update</param>
        /// <param name="RequestCode">Optional Guid string</param>
        /// <param name="TicketId">Optional ticket Id</param>
        public UpdateDepositTicketResponse UpdateDepositTicket(int AccountId, string status, decimal amount, string RequestCode = null, int? TicketId = null)
        {
            UpdateDepositTicketResponse result = new UpdateDepositTicketResponse();
            try
            {
                var oldDeposit = GetDepositTicket(AccountId, RequestCode, TicketId);
                oldDeposit.Amount = amount;
                oldDeposit.Status = String.IsNullOrEmpty(status) ? oldDeposit.Status : status;

                Frame f = GetRequestFrame("UpdateDepositTicket");
                AlphaPointDepositTicket deposit = new AlphaPointDepositTicket();
                deposit = oldDeposit;
                result = GetResponseSingle<UpdateDepositTicketResponse>(f, deposit);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Non usable public methods
        /// <summary>
        /// Authentificate user connection to WebSocket
        /// </summary>
        /// <param name="ApiKey">Public API Key</param>
        /// <param name="ApiSecretKey">API Secret Key</param>
        /// <param name="ClientId">Id of current client</param>
        /// <returns></returns>
        [Obsolete("This method is obsolete. You should use method AuthenticateUser(string userName, string password) for authorization.")]
        public AuthenticateUserResponse AuthenticateUser(string ApiKey, string ApiSecretKey, int ClientId)
        {
            try
            {
                Frame f = GetRequestFrame("AuthenticateUser");

                //Nonce is a regular integer number. It must increase with every request you make. A common practice is to use unix time for this parameter.
                var unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                var nonce = unixTimestamp;

                string signature = GenerateSignature(ApiKey, ApiSecretKey, ClientId, nonce);

                KeysAuthModel model = new KeysAuthModel() { APIKey = ApiKey, Nonce = nonce, Signature = signature };
                var result = GetResponseSingle<AuthenticateUserResponse>(f, model);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the available OMS's on the system you're connected to
        /// </summary>
        /// <param name="OperatorId">Identifier of current operator</param>
        /// <returns></returns>
        public List<AvailableOMS> GetOMSs(int OperatorId)
        {
            List<AvailableOMS> result = new List<AvailableOMS>();
            try
            {
                Frame f = GetRequestFrame("GetOMSs");
                GetOMSsParameterModel model = new GetOMSsParameterModel() { OperatorId = OperatorId };
                result = GetResponseList<AvailableOMS>(f, model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Requests a list of available Products from the API.
        /// </summary>
        /// <returns></returns>
        public List<AlphaPointProduct> GetProducts()
        {
            List<AlphaPointProduct> resultList = new List<AlphaPointProduct>();
            try
            {
                Frame f = GetRequestFrame("GetProducts");
                GetProductsParametersModel parameters = new GetProductsParametersModel() { OMSId = 1 };
                resultList = GetResponseList<AlphaPointProduct>(f, parameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultList;
        }

        /// <summary>
        /// Requests a single product's information from the API.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public AlphaPointProduct GetProduct(int productId)
        {
            AlphaPointProduct resultList = new AlphaPointProduct();
            try
            {
                Frame f = GetRequestFrame("GetProduct");
                GetProductParametersModel parameters = new GetProductParametersModel() { OMSId = 1, ProductId = productId };
                resultList = GetResponseSingle<AlphaPointProduct>(f, parameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultList;
        }

        /// <summary>
        /// Requests a list of available trading instruments (pairs) from the API.
        /// </summary>
        /// <returns></returns>
        public List<AlphaPointInstrument> GetInstruments()
        {
            List<AlphaPointInstrument> resultList = new List<AlphaPointInstrument>();
            try
            {
                Frame f = GetRequestFrame("GetInstruments");
                GetInstrumentsParametersModel parameters = new GetInstrumentsParametersModel() { OMSId = 1 };
                resultList = GetResponseList<AlphaPointInstrument>(f, parameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultList;
        }

        /// <summary>
        /// Gets the current state of all Deposit Tickets for an account
        /// </summary>
        /// <param name="AccountId">Account identifier</param>
        /// <returns></returns>
        public List<AlphaPointDepositTicket> GetDepositTickets(int AccountId)
        {
            List<AlphaPointDepositTicket> resultList = new List<AlphaPointDepositTicket>();
            try
            {
                Frame f = GetRequestFrame("GetDepositTickets");
                GetDepositTicketsParametersModel parameters = new GetDepositTicketsParametersModel() { OMSId = 1, AccountId = AccountId, Operatorid = 1 };
                resultList = GetResponseList<AlphaPointDepositTicket>(f, parameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultList;
        }

        /// <summary>
        /// Get a matching deposit ticket by request code or ticket id
        /// </summary>
        /// <param name="AccountId">User account Id</param>
        /// <param name="RequestCode">Optional Guid string</param>
        /// <param name="TicketId">Optional ticket Id</param>
        /// <returns></returns>
        public AlphaPointDepositTicket GetDepositTicket(int AccountId, string RequestCode = null, int? TicketId = null)
        {
            AlphaPointDepositTicket result = new AlphaPointDepositTicket();
            try
            {
                Frame f = GetRequestFrame("GetDepositTicket");
                GetDepositTicketParametersModel parameters = new GetDepositTicketParametersModel() { OMSId = 1, AccountId = AccountId, RequestCode = RequestCode, TicketId = TicketId };
                result = GetResponseSingle<AlphaPointDepositTicket>(f, parameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the current state of all Deposit Tickets matching provided fields. Fields are filtered with and logic based on what is included or not in query.
        /// </summary>
        /// <param name="AccountId">User account Id. Optional</param>
        /// <param name="Status">Deposit ticket status. Optional</param>
        /// <param name="TicketId">Ticket Id. Optional</param>
        /// <param name="Limit">Limit. Optional</param>
        /// <param name="Username">Name of user. Optional</param>
        /// <param name="StartTimestamp">Start time to matching. Optional</param>
        /// <param name="EndTimestamp">End time to matching. Optional</param>
        /// <param name="Amount">Amount of the ticket. Optional</param>
        /// <param name="AmountOperator">Operator to compare amount. Optional - required with amount. Values are 0: '=', 1: 'amount>=value', 2: 'amount<=value'</param>
        /// <returns></returns>
        public List<AlphaPointDepositTicket> GetAllDepositTickets(int? AccountId = null, string Status = null, int? TicketId = null, int? Limit = null,
            string Username = null, DateTime? StartTimestamp = null, DateTime? EndTimestamp = null, decimal? Amount = null, int? AmountOperator = null)
        {
            List<AlphaPointDepositTicket> list = new List<AlphaPointDepositTicket>();
            try
            {
                Frame f = GetRequestFrame("GetAllDepositTickets");
                GetAllDepositTicketsParametersModel parameters = new GetAllDepositTicketsParametersModel(AccountId, Status, TicketId, Limit, Username, StartTimestamp, EndTimestamp, Amount, AmountOperator);
                list = GetResponseList<AlphaPointDepositTicket>(f, parameters);
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the AccountInfo for the requested account, if the current session is authorized to see the account information
        /// </summary>
        /// <param name="accountID">Account Id</param>
        /// <returns></returns>
        public string GetAccountInfo(int accountID)
        {
            string result = "";
            try
            {
                //get deposit info
                Frame f = GetRequestFrame("GetAccountInfo");
                GetAccountInfoParametersModel model = new GetAccountInfoParametersModel() { OMSId = 1, AccountId = accountID };
                result = GetResponseSingle<string>(f, model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    LogOut();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }

}
